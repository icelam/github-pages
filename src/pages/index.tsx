import type { FC } from 'react';
import { type HeadProps, Link, graphql } from 'gatsby';
import styled, { css } from 'styled-components';
import {
  Card, FlexRow, FlexColumn, SEO,
} from '../components';
import routes from '../routes';
import { PageProps } from '../types';
import { withErrorBoundary } from '../hoc';

interface IndexPageData {
  allMdx: {
    nodes: {
      frontmatter: Pick<Queries.MdxFrontmatter, 'projectId' | 'projectName' | 'projectImage'>;
    }[];
    totalCount: number;
  };
}

export type IndexPageProps = PageProps<IndexPageData>;

const hoverOrFocusedStyle = css`
  transform: translateY(calc(var(--var-pixel-size) * -3));
`;

const LinkWithHoverAnimation = styled(Link)`
  > div {
    transition: all 150ms;
  }

  @media (pointer: fine) {
    &:hover > div {
      ${hoverOrFocusedStyle}
    }
  }

  &:focus-visible > div {
    ${hoverOrFocusedStyle}
  }
`;

const IndexPage: FC<IndexPageProps> = ({ data }) => {
  const { allMdx: { nodes: projects } } = data;

  return (
    <FlexRow>
      {
        projects.map(({ frontmatter: info }, index) => (
          <FlexColumn $xs={12} $sm={6} $md={6} $lg={4} key={info.projectId}>
            <LinkWithHoverAnimation to={`${routes.projectDetailsRoot}/${info.projectId}`}>
              <Card
                image={info.projectImage.childImageSharp!.gatsbyImageData}
                imageAlt={`Key visual of ${info.projectName}`}
                imageLazyLoad={index > 3}
              >
                {info.projectName}
              </Card>
            </LinkWithHoverAnimation>
          </FlexColumn>
        ))
      }
    </FlexRow>
  );
};

export default withErrorBoundary<IndexPageProps>(IndexPage);

export const Head: FC<HeadProps> = (props) => <SEO {...props} />;

export const pageQuery = graphql`
  {
    allMdx(
      filter: { fields: { collection: { eq: "projects" } } }
      sort: { frontmatter: { projectDate: DESC } }
    ) {
      nodes {
        frontmatter {
          projectId
          projectName
          projectImage {
            childImageSharp {
              gatsbyImageData(
                layout: FULL_WIDTH
                quality: 100
                placeholder: BLURRED
                blurredOptions: { width: 20 }
                sizes: "(max-width: 575px) 100vw, (max-width: 991px) 50vw, (min-width: 992px) 34vw, 100vw"
                breakpoints: [390, 450, 575, 780, 900, 1150]
              )
            }
          }
          projectDate
        }
      }
      totalCount
    }
  }
`;
