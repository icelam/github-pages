import type { FC } from 'react';
import { type HeadProps, Link } from 'gatsby';
import { PageProps } from '../types';
import routes from '../routes';
import { withErrorBoundary } from '../hoc';
import { SEO, UnexpectedError } from '../components';

const NotFoundPage: FC<PageProps> = () => (
  <UnexpectedError errorCode="404" errorTitle="Page Not Found">
    <p>
      Sorry, the page you are looking for could not be found. You can
      {' '}
      <Link to={routes.home}>click here</Link>
      {' '}
      to head to homepage.
    </p>
  </UnexpectedError>
);

export default withErrorBoundary(NotFoundPage);

export const Head: FC<HeadProps> = (props) => (
  <SEO {...props} options={{ title: 'Page Not Found' }} />
);
