/**
 * IMPORTANT: This file will be imported using raw-loader to use in script tag of gatsby.ssr.tsx.
 * Do NOT use import statement or Typescript in this file!
 *
 * This files aims to handle flickering of dark themes, the code is based on Dan's blog:
 * https://github.com/gaearon/overreacted.io/blob/2e810e189ba30c9825b989c3cf678a491ee2c1c3/src/html.js#L21
 */

(function ssrThemeHandler() {
  const STORAGE_KEY = 'theme';
  let preferredTheme = window.localStorage.getItem(STORAGE_KEY);

  const setPreferredTheme = (newTheme) => {
    window.__GATSBY_THEME__ = newTheme;
    document.documentElement.setAttribute(`data-${STORAGE_KEY}`, newTheme);
    preferredTheme = newTheme;
  };

  const persistThemePreference = (theme) => {
    window.localStorage.setItem('theme', theme);
  };

  const darkQuery = window.matchMedia('(prefers-color-scheme: dark)');

  const handleMediaChange = (event) => {
    const newTheme = event.matches ? 'dark' : 'light';
    setPreferredTheme(newTheme);
    persistThemePreference(newTheme);
  };

  if (darkQuery.addEventListener) {
    darkQuery.addEventListener('change', handleMediaChange);
  } else {
    // Fallback to addListener which is deprecated on newer browsers, where older browsers like
    // Safari on iOS 13 does not support addEventListener
    darkQuery.addListener(handleMediaChange);
  }

  setPreferredTheme(preferredTheme || (darkQuery.matches ? 'dark' : 'light'));

  // Expose function to allow theme changes from React
  window.__toggleTheme = (theme) => {
    setPreferredTheme(theme);
    persistThemePreference(theme);
  };
}());
