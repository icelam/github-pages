import type { ComponentType, FC } from 'react';
import type { PageProps } from '../types';
import { ErrorBoundary } from '../components';

const withErrorBoundary = <T extends PageProps<Record<string, any>> = PageProps>(
  Component: ComponentType<T>,
) => {
  const ComputedComponent: FC<T> = (props) => (
    <ErrorBoundary>
      <Component {...props} />
    </ErrorBoundary>
  );

  return ComputedComponent;
};

export default withErrorBoundary;
