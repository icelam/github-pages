import type { FC } from 'react';
import { type HeadProps, graphql } from 'gatsby';
import { PageProps, MdxPageData, MdxPageSEOFrontmatter } from '../types';
import { withErrorBoundary } from '../hoc';
import { MDXProvider } from '../providers';
import { SEO } from '../components';

type DefaultTemplateData = MdxPageData<MdxPageSEOFrontmatter>;

export type DefaultTemplateProps = PageProps<DefaultTemplateData>;

// Template for markdown contents
// Docs: https://www.gatsbyjs.com/docs/how-to/routing/adding-markdown-pages
const DefaultTemplate: FC<DefaultTemplateProps> = ({ children }) => (
  <MDXProvider>{children}</MDXProvider>
);

// `gatsby-plugin-mdx` fails to render correctly if we directly use HOC on default export
const DefaultTemplateWithErrorBoundary = withErrorBoundary<DefaultTemplateProps>(
  DefaultTemplate,
);
export default DefaultTemplateWithErrorBoundary;

export const Head: FC<HeadProps> = (props) => <SEO {...props} />;

export const pageQuery = graphql`
  query DefaultTemplate($id: String!) {
    mdx(id: {eq: $id}) {
      frontmatter {
        pageTitle
        pageDescription
        pageKeywords
      }
    }
  }
`;
