import type { FC } from 'react';
import styled from 'styled-components';
import { type HeadProps, graphql } from 'gatsby';
import { SEO, Tag } from '../components';
import techStackData from '../data/techStack';
import { MdxPageData, PageProps } from '../types';
import { withErrorBoundary } from '../hoc';
import { MDXProvider } from '../providers';

import { ReactComponent as ProjectLinkIcon } from '../assets/images/icons/website.svg';
import { ReactComponent as GithubLinkIcon } from '../assets/images/icons/github.svg';
import { ReactComponent as DateIcon } from '../assets/images/icons/date.svg';

type ProjectDetailsTemplateFrontmatter = Pick<
  Queries.MdxFrontmatter,
  | 'pageTitle'
  | 'pageDescription'
  | 'pageKeywords'
  | 'projectId'
  | 'projectName'
  | 'projectNameTranslation'
  | 'projectLinks'
  | 'projectDate'
  | 'githubLink'
  | 'techStack'
>;

type ProjectDetailsTemplateData = MdxPageData<ProjectDetailsTemplateFrontmatter>;

export type ProjectDetailsTemplateProps = PageProps<ProjectDetailsTemplateData>;

const ProjectTitle = styled.h1`
  margin-bottom: 0.5625rem;
`;

const ProjectInfo = styled.div`
  margin: 1.6875rem 0;
  font-size: 0.875rem;
  line-height: 1.312m5re;
  color: var(--color-link);

  & > div {
    margin: 0 0 0.25rem 0;
    display: flex;
    justify-content: flex-start;
    align-items: flex-start;

    &:last-of-type {
      margin: 0;
    }

    svg {
      width: 1rem;
      height: 1rem;
      min-width: 1rem;
      max-height: 1rem;
      vertical-align: middle;
      margin-right: 0.5rem;
      image-rendering: -webkit-optimize-contrast;
      color: var(--color-main-text);
    }

    a, span {
      vertical-align: middle;
      line-height: 1rem;
      word-break: break-all;
    }

    a:first-child {
      margin-left 24px;
    }
  }
`;

// Template for markdown contents
// Docs: https://www.gatsbyjs.com/docs/how-to/routing/adding-markdown-pages
const ProjectDetailsTemplate: FC<ProjectDetailsTemplateProps> = ({
  data, children,
}) => {
  const {
    projectName,
    projectNameTranslation,
    projectLinks,
    projectDate,
    githubLink,
    techStack,
  } = data.mdx?.frontmatter ?? {};

  return (
    <>
      <ProjectTitle>
        {projectName}
        {/* eslint-disable-next-line react/jsx-one-expression-per-line */}
        {projectNameTranslation && (<>{' '}({projectNameTranslation})</>)}
      </ProjectTitle>

      {
        techStack?.map((techName) => {
          const stackInfo = techStackData[techName];

          return (
            <Tag
              $backgroundColor={stackInfo.backgroundColor}
              $textColor={stackInfo.textColor}
              key={stackInfo.name}
            >
              {stackInfo.name}
            </Tag>
          );
        })
      }

      <ProjectInfo>
        {projectDate && (
          <div>
            <DateIcon />
            <span>{new Intl.DateTimeFormat('en-GB', { dateStyle: 'full' }).format(new Date(projectDate))}</span>
          </div>
        )}

        {projectLinks && (
          projectLinks.map((link, index) => (
            <div key={link}>
              {index === 0 ? <ProjectLinkIcon /> : null }
              <a href={link} target="_blank" rel="noopener noreferrer">{link}</a>
            </div>
          ))
        )}

        {githubLink && (
          <div>
            <GithubLinkIcon />
            <a href={githubLink} target="_blank" rel="noopener noreferrer">{githubLink}</a>
          </div>
        )}
      </ProjectInfo>

      <MDXProvider>
        {children}
      </MDXProvider>
    </>
  );
};

// `gatsby-plugin-mdx` fails to render correctly if we directly use HOC on default export
const ProjectDetailsPageWithErrorBoundary = withErrorBoundary<ProjectDetailsTemplateProps>(
  ProjectDetailsTemplate,
);
export default ProjectDetailsPageWithErrorBoundary;

export const Head: FC<HeadProps> = (props) => <SEO {...props} />;

export const pageQuery = graphql`
  query ProjectDetailsTemplate($id: String!) {
    mdx(
      fields: { collection: { eq: "projects" } }
      id: { eq: $id }
    ) {
      frontmatter {
        pageTitle
        pageDescription
        pageKeywords
        projectId
        projectName
        projectNameTranslation
        projectLinks
        projectDate
        githubLink
        techStack
      }
    }
  }
`;
