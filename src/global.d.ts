import { AvailableThemes } from './types';

declare global {
  interface Window {
    __GATSBY_THEME__: AvailableThemes;
    __toggleTheme: (theme: AvailableThemes) => void;
  }
}
