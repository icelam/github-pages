import type { FC, ReactNode } from 'react';

interface HTMLProps {
  htmlAttributes: Record<string, any>;
  headComponents: ReactNode[];
  bodyAttributes: Record<string, any>;
  body: string;
  preBodyComponents: ReactNode[];
  postBodyComponents: ReactNode[];
}

// Responsible for everything other than components that lives in the <body />
// Docs: https://www.gatsbyjs.com/docs/conceptual/building-with-components/#html-component
// Example: https://github.com/gatsbyjs/gatsby/blob/master/examples/functions-auth0/src/html.js
const HTML: FC<HTMLProps> = ({
  htmlAttributes, headComponents, bodyAttributes, body, preBodyComponents, postBodyComponents,
}) => (
  <html lang="en" {...htmlAttributes}>
    <head>
      <meta charSet="utf-8" />
      <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="language" content="English" />
      <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
      <meta name="viewport" content="width=device-width, initial-scale=1" />

      <meta name="format-detection" content="telephone=no" />
      <meta name="format-detection" content="date=no" />
      <meta name="format-detection" content="address=no" />
      <meta name="format-detection" content="email=no" />

      {
        /**
          * The below tags will be added using React Helmet:
          * <title>
          * <meta name="description" />
          * <meta name="keywords" />
          * <meta name="title" />
          * <meta name="robots" />
          * <meta property="og:type"
          * <meta name="og:description" />
          * <meta name="og:title" />
          * <meta name="og:url" />
          * <meta property="og:image" />
          * <meta property="og:image:secure_url" />
          * <meta property="og:image:type" />
          * <meta property="og:image:width" />
          * <meta property="og:image:height" />
          * <meta property="og:image:alt" />
          * <meta property="og:site_name" />
          */
      }

      <meta name="apple-mobile-web-app-status-bar-style" content="black" />
      <meta name="apple-mobile-web-app-capable" content="yes" />

      <link rel="icon" href="/favicon-dark.ico?v=1" media="(prefers-color-scheme: dark)" />
      <link rel="icon" href="/favicon.ico?v=1" media="(prefers-color-scheme: light)" />

      {headComponents}
    </head>
    <body {...bodyAttributes}>
      {preBodyComponents}
      {/* eslint-disable-next-line react/no-danger */}
      <div id="___gatsby" dangerouslySetInnerHTML={{ __html: body }} />
      {postBodyComponents}
    </body>
  </html>
);

export default HTML;
