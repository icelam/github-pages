const timeDifference = (toDate: Date, fromDate: Date): string => {
  if (!(typeof toDate.getTime === 'function' && typeof fromDate.getTime === 'function')) {
    return '';
  }

  const msDiff = Math.abs(toDate.getTime() - fromDate.getTime());

  const seconds = Math.round(msDiff / 1000);
  const minutes = Math.round(seconds / 60);
  const hours = Math.round(minutes / 60);
  const days = Math.round(hours / 24);
  const months = Math.round(days / 30);

  const displayMonths = (days - (days % 30)) / 30 + ((days % 30) >= 15 ? 0.5 : 0);
  const displayYear = (months - (months % 12)) / 12 + ((months % 12) >= 6 ? 0.5 : 0);

  if (msDiff === 0) {
    return '0 second';
  }

  if (minutes <= 1) {
    return '1 minute';
  }

  if (minutes < 60) {
    return `${minutes} minutes`;
  }

  if (hours < 24) {
    return `${hours} hour${hours > 1 ? 's' : ''}`;
  }

  if (days < 30) {
    return `${days} day${days > 1 ? 's' : ''}`;
  }

  if (days < 365) {
    return `${displayMonths} month${displayMonths > 1 ? 's' : ''}`;
  }

  return `${displayYear} year${displayYear > 1 ? 's' : ''}`;
};

export default timeDifference;
