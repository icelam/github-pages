import timeDifference from '../time';

describe('timeDifference()', () => {
  test('should return "0 second" for start date 2015-10-08 and end date 2015-10-08', () => {
    expect(timeDifference(new Date('2015-10-08'), new Date('2015-10-08'))).toBe('0 second');
  });

  test('should return "1 minute" for start date 2015-10-08 00:00:00 and end date 2015-10-08 00:00:30', () => {
    expect(timeDifference(new Date('2015-10-08T00:00:00'), new Date('2015-10-08T00:00:30'))).toBe('1 minute');
  });

  test('should return "32 minutes" for start date 2015-10-08 00:00:00 and end date 2015-10-08 00:32:00', () => {
    expect(timeDifference(new Date('2015-10-08T00:00:00'), new Date('2015-10-08T00:32:00'))).toBe('32 minutes');
  });

  test('should return "1 hour" for start date 2015-10-08 00:00:00 and end date 2015-10-08 01:02:00', () => {
    expect(timeDifference(new Date('2015-10-08T00:00:00'), new Date('2015-10-08T01:02:00'))).toBe('1 hour');
  });

  test('should return "3 hours" for start date 2015-10-08 00:00:00 and end date 2015-10-08 03:42:00', () => {
    expect(timeDifference(new Date('2015-10-08T00:00:00'), new Date('2015-10-08T03:42:00'))).toBe('4 hours');
  });

  test('should return "1 day" for start date 2015-10-08 and end date 2015-10-07', () => {
    expect(timeDifference(new Date('2015-10-07'), new Date('2015-10-08'))).toBe('1 day');
  });

  test('should return "10 days" for start date 2015-10-08 and end date 2015-10-18', () => {
    expect(timeDifference(new Date('2015-10-18'), new Date('2015-10-08'))).toBe('10 days');
  });

  test('should return "1 month" for start date 2015-10-08 and end date 2015-11-08', () => {
    expect(timeDifference(new Date('2015-11-08'), new Date('2015-10-08'))).toBe('1 month');
  });

  test('should return "2.5" months for start date 2015-10-08 and end date 2015-12-23', () => {
    expect(timeDifference(new Date('2015-12-23'), new Date('2015-10-08'))).toBe('2.5 months');
  });

  test('should return "10 months" for start date 2020-10-08 and end date 2019-12-01', () => {
    expect(timeDifference(new Date('2019-12-01'), new Date('2020-10-08'))).toBe('10 months');
  });

  test('should return "1 year" for start date 2015-10-08 and end date 2016-10-08', () => {
    expect(timeDifference(new Date('2016-10-08'), new Date('2015-10-08'))).toBe('1 year');
  });

  test('should return "4 years" for start date 2015-10-08 and end date 2019-12-01', () => {
    expect(timeDifference(new Date('2019-12-01'), new Date('2015-10-08'))).toBe('4 years');
  });

  test('should return "4.5 years" for start date 2015-10-08 and end date 2020-03-01', () => {
    expect(timeDifference(new Date('2020-03-01'), new Date('2015-10-08'))).toBe('4.5 years');
  });

  test('should return empty string for any invalid inputs', () => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore - force incorrect data type to test for error handling logics
    expect(timeDifference('aaa', new Date('2015-10-08'))).toBe('');
  });

  test('should return empty string if both inputs are invalid', () => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore - force incorrect data type to test for error handling logics
    expect(timeDifference('aaa', 'bbb')).toBe('');
  });
});
