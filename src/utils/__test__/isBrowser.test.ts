import isBrowser from '../isBrowser';

describe('isBrowser()', () => {
  test('should return false when window is undefined', () => {
    const windowSpy = jest.spyOn(global, 'window', 'get');
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore - force window to be undefined
    windowSpy.mockImplementationOnce(() => undefined);

    expect(isBrowser()).toBe(false);
  });

  test('should return true when window is defined', () => {
    expect(isBrowser()).toBe(true);
  });
});
