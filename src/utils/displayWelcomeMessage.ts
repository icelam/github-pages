/**
 * Base64 string of console-welcome-animation.svg.
 * Note that Firefox only support displaying base64 SVG that is around length 9000 - 10000
 */
const base64Svg = 'data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTE0IiBoZWlnaHQ9IjExNCIgdmlld0JveD0iMCAwIDExNCAxMTQiIGZpbGw9Im5vbmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PGc+PHBhdGggZD0iTTQ0IDQ2VjIwSDQ4VjE2SDUyVjEySDYwVjhINzhWMTJIODZWMTZIOTBWMjBIOTRWNDZIOTBWNTRIODZWNThINzhWNjBIODhWNTZIOTJWNTJIMTAyVjYySDk4VjY2SDk0VjcwSDkwVjg2SDg2VjkwSDgyVjk0SDc4Vjk4SDcwVjEwNEg4MlYxMTRINzZWMTEwSDY2VjExNEg2MFYxMTBINTZWMTA2SDQ0Vjk2SDQ4Vjk0SDMwVjEwOEg1NFYxMTRIMTZWODZIMTJWODBIMTZWNzZIMjBWNzJIMjRWNDRIMzBWODBIMzZWNjhINTJWNjRINTZWNjBINjBWNThINTJWNTRINDhWNDZINDRaIiBmaWxsPSJ3aGl0ZSIgZmlsbC1vcGFjaXR5PSIwLjUiIC8+PHBhdGggZD0iTTUgMTVINFYyOUg1VjMwSDZWMzFIMzZWMzBIMzdWMjlIMzhWMjZIMzlWMjVINDBWMjRINDFWMjBINDBWMTlIMzlWMThIMzhWMTVIMzdWMTRIMzZWMTNINlYxNEg1VjE1WiIgZmlsbD0id2hpdGUiIGZpbGwtb3BhY2l0eT0iMC41IiAvPjxnIGlkPSJkZXNrIj48cmVjdCB4PSIyMSIgeT0iODkiIHdpZHRoPSI4IiBoZWlnaHQ9IjI0IiBmaWxsPSIjNUE0ODM2IiAvPjxyZWN0IHg9Ijc3IiB5PSIxMDUiIHdpZHRoPSI0IiBoZWlnaHQ9IjgiIGZpbGw9IiM1QTQ4MzYiIC8+PHJlY3QgeD0iNjEiIHk9IjEwNSIgd2lkdGg9IjIwIiBoZWlnaHQ9IjQiIGZpbGw9IiM4ODZDNTQiIC8+PHJlY3QgeD0iNjEiIHk9IjEwNSIgd2lkdGg9IjQiIGhlaWdodD0iOCIgZmlsbD0iIzVBNDgzNiIgLz48cmVjdCB4PSI3NyIgeT0iNjkiIHdpZHRoPSIxMiIgaGVpZ2h0PSIxNiIgZmlsbD0iIzVBNDgzNiIgLz48cGF0aCBkPSJNODEgOTNINTdWNjVIODVWODlIODFWOTNaIiBmaWxsPSIjODg2QzU0IiAvPjxwYXRoIGQ9Ik0yNSA3M1Y0NUgyOVY4NUgxN1Y3N0gyMVY3M0gyNVoiIGZpbGw9IiMzNDM0MzQiIC8+PHBhdGggZD0iTTE3IDExM1Y4MUg0OVY5M0gyNVYxMDlINTNWMTEzSDE3WiIgZmlsbD0iIzg4NkM1NCIgLz48cmVjdCB4PSIxMyIgeT0iODEiIHdpZHRoPSI0MCIgaGVpZ2h0PSI0IiBmaWxsPSIjNUE0ODM2IiAvPjxyZWN0IHg9IjYxIiB5PSI4OSIgd2lkdGg9IjE2IiBoZWlnaHQ9IjgiIGZpbGw9IiM1QTQ4MzYiIC8+PC9nPjxnIGlkPSJjaGFyYWN0ZXIiPjxwYXRoIGZpbGwtcnVsZT0iZXZlbm9kZCIgY2xpcC1ydWxlPSJldmVub2RkIiBkPSJNMzcgODFINTNWNzdINTdWODVINTNWODlINDlWOTdINDVWMTA1SDU3VjEwOUg2OVY5N0g3M1Y5M0g3N1Y4OUg4MVY3M0g4OVY2OUg5M1Y2NUg5N1Y2MUgxMDFWNTNIOTNWNTdIODlWNjFINTdWNjVINTNWNjlIMzdWODFaTTYxIDkzSDU3VjEwMUg2MVY5M1pNNjEgOTNINjVWODlINjFWOTNaIiBmaWxsPSIjMDMwMzAzIiAvPjxwYXRoIGQ9Ik00NSAyMVY0NUg0OVY1M0g1M1Y1N0g2MVY2MUg3N1Y1N0g4NVY1M0g4OVY0NUg5M1YyMUg4OVYxN0g4NVYxM0g3N1Y5SDYxVjEzSDUzVjE3SDQ5VjIxSDQ1WiIgZmlsbD0iIzAzMDMwMyIgLz48cGF0aCBkPSJNNTMgNDFINDlWNDVINTNWNDlINTdWNTNINjFWNTdINjVWNjFINzNWNTdINzdWNTNIODFWNDlIODVWNDVIODlWNDFIODVWMjlIODFWMzNINzdWMjlINzNWMzNINTdWMzdINTNWNDFaIiBmaWxsPSIjRkVFNEM5IiAvPjxwYXRoIGlkPSJWZWN0b3IgNyIgZD0iTTQ5IDIxVjM3SDUzVjMzSDU3VjI5SDY5VjI1SDg1VjI5SDg5VjIxSDg1VjE3SDc3VjEzSDYxVjE3SDUzVjIxSDQ5WiIgZmlsbD0iIzM0MzQzNCIgLz48cmVjdCB4PSI1NyIgeT0iMzciIHdpZHRoPSI0IiBoZWlnaHQ9IjQiIGZpbGw9IiMwMzAzMDMiIC8+PHJlY3QgeD0iNzciIHk9IjM3IiB3aWR0aD0iNCIgaGVpZ2h0PSI0IiBmaWxsPSIjMDMwMzAzIiAvPjxyZWN0IHg9IjY1IiB5PSI0OSIgd2lkdGg9IjgiIGhlaWdodD0iNCIgZmlsbD0iIzAzMDMwMyIgLz48Zz48cmVjdCB4PSI3MyIgeT0iNTciIHdpZHRoPSI0IiBoZWlnaHQ9IjQiIGZpbGw9IiMzNDM0MzQiIC8+PHJlY3QgeD0iNzMiIHk9IjU3IiB3aWR0aD0iNCIgaGVpZ2h0PSI0IiBmaWxsPSIjMzQzNDM0IiAvPjwvZz48Zz48cmVjdCB4PSI2MSIgeT0iNTciIHdpZHRoPSI0IiBoZWlnaHQ9IjQiIGZpbGw9IiMzNDM0MzQiIC8+PHJlY3QgeD0iNjEiIHk9IjU3IiB3aWR0aD0iNCIgaGVpZ2h0PSI0IiBmaWxsPSIjMzQzNDM0IiAvPjwvZz48cmVjdCB4PSI0MSIgeT0iNzMiIHdpZHRoPSIxMiIgaGVpZ2h0PSI0IiBmaWxsPSJ3aGl0ZSIgLz48cmVjdCB4PSI1MyIgeT0iNjkiIHdpZHRoPSI0IiBoZWlnaHQ9IjQiIGZpbGw9IndoaXRlIiAvPjxyZWN0IHg9IjYxIiB5PSI2NSIgd2lkdGg9IjE2IiBoZWlnaHQ9IjE2IiBmaWxsPSJ3aGl0ZSIgLz48cmVjdCB4PSI4MSIgeT0iNjUiIHdpZHRoPSI4IiBoZWlnaHQ9IjQiIGZpbGw9IndoaXRlIiAvPjxyZWN0IHg9Ijg5IiB5PSI2MSIgd2lkdGg9IjQiIGhlaWdodD0iNCIgZmlsbD0id2hpdGUiIC8+PHJlY3QgeD0iOTMiIHk9IjU3IiB3aWR0aD0iNCIgaGVpZ2h0PSI0IiBmaWxsPSIjRkVFNEM5IiAvPjxyZWN0IHg9IjQxIiB5PSI3MyIgd2lkdGg9IjQiIGhlaWdodD0iNCIgZmlsbD0iI0ZFRTRDOSIgLz48cmVjdCB4PSI0NSIgeT0iOTciIHdpZHRoPSIxMiIgaGVpZ2h0PSI4IiBmaWxsPSIjMjkxNzEwIiAvPjxyZWN0IHg9IjQ5IiB5PSI5NyIgd2lkdGg9IjQiIGhlaWdodD0iNCIgZmlsbD0iIzNCMjIxOCIgLz48cmVjdCB4PSI1NyIgeT0iMTAxIiB3aWR0aD0iMTIiIGhlaWdodD0iOCIgZmlsbD0iIzI5MTcxMCIgLz48cmVjdCB4PSI2MSIgeT0iMTAxIiB3aWR0aD0iNCIgaGVpZ2h0PSI0IiBmaWxsPSIjM0IyMjE4IiAvPjwvZz48ZyBpZD0iZGlhbG9nIj48cGF0aCBpZD0iYmFja2dyb3VuZCIgZD0iTTUgMjhWMTZINlYxNUg3VjE0SDM1VjE1SDM2VjE2SDM3VjE5SDM4VjIwSDM5VjIxSDQwVjIzSDM5VjI0SDM4VjI1SDM3VjI4SDM2VjI5SDM1VjMwSDdWMjlINlYyOEg1WiIgZmlsbD0id2hpdGUiIC8+PGcgaWQ9ImJvcmRlciI+PHBhdGggZD0iTTUgMTZINlYyOEg1VjE2WiIgZmlsbD0iIzAzMDMwMyIgLz48cGF0aCBkPSJNNiAyOEg3VjI5SDZWMjhaIiBmaWxsPSIjMDMwMzAzIiAvPjxwYXRoIGQ9Ik03IDI5SDM1VjMwSDdWMjlaIiBmaWxsPSIjMDMwMzAzIiAvPjxwYXRoIGQ9Ik02IDE1SDdWMTZINlYxNVoiIGZpbGw9IiMwMzAzMDMiIC8+PHBhdGggZD0iTTcgMTRIMzVWMTVIN1YxNFoiIGZpbGw9IiMwMzAzMDMiIC8+PHBhdGggZD0iTTM1IDE1SDM2VjE2SDM1VjE1WiIgZmlsbD0iIzAzMDMwMyIgLz48cGF0aCBkPSJNMzYgMTZIMzdWMTlIMzZWMTZaIiBmaWxsPSIjMDMwMzAzIiAvPjxwYXRoIGQ9Ik0zNyAxOUgzOFYyMEgzN1YxOVoiIGZpbGw9IiMwMzAzMDMiIC8+PHBhdGggZD0iTTM4IDIwSDM5VjIxSDM4VjIwWiIgZmlsbD0iIzAzMDMwMyIgLz48cGF0aCBkPSJNMzkgMjFINDBWMjNIMzlWMjFaIiBmaWxsPSIjMDMwMzAzIiAvPjxwYXRoIGQ9Ik0zOCAyM0gzOVYyNEgzOFYyM1oiIGZpbGw9IiMwMzAzMDMiIC8+PHBhdGggZD0iTTM3IDI0SDM4VjI1SDM3VjI0WiIgZmlsbD0iIzAzMDMwMyIgLz48cGF0aCBkPSJNMzYgMjVIMzdWMjhIMzZWMjVaIiBmaWxsPSIjMDMwMzAzIiAvPjxwYXRoIGQ9Ik0zNSAyOEgzNlYyOUgzNVYyOFoiIGZpbGw9IiMwMzAzMDMiIC8+PC9nPjxnIGlkPSJIZWxsbyBXb3JsZCEiPjxnIGlkPSJFeGNsYWltYXRpb24gTWFyayIgb3BhY2l0eT0iMCI+PHBhdGggZD0iTTMzIDI2VjIzSDM0VjI2SDMzWiIgZmlsbD0iIzAzMDMwMyIgLz48cGF0aCBkPSJNMzMgMjhWMjdIMzRWMjhIMzNaIiBmaWxsPSIjMDMwMzAzIiAvPjxhbmltYXRlIGlkPSJTeW1ib2xfQSIgYXR0cmlidXRlTmFtZT0ib3BhY2l0eSIgdmFsdWVzPSIwOzE7IiBiZWdpbj0iRDFfQS5lbmQiIGR1cj0iMC4xMjVzIiBmaWxsPSJmcmVlemUiIC8+PGFuaW1hdGUgYXR0cmlidXRlTmFtZT0ib3BhY2l0eSIgdmFsdWVzPSIxOzA7IiBiZWdpbj0iU3ltYm9sX0EuZW5kKzFzOyIgZHVyPSIwLjEyNXMiIGZpbGw9ImZyZWV6ZSIgLz48L2c+PHBhdGggaWQ9IkQxIiBmaWxsLXJ1bGU9ImV2ZW5vZGQiIGNsaXAtcnVsZT0iZXZlbm9kZCIgZD0iTTI4IDIzVjI4SDMxVjI3SDMyVjI0SDMxVjIzSDI4Wk0zMSAyNEgyOVYyN0gzMVYyNFoiIGZpbGw9IiMwMzAzMDMiIG9wYWNpdHk9IjAiPjxhbmltYXRlIGlkPSJEMV9BIiBhdHRyaWJ1dGVOYW1lPSJvcGFjaXR5IiB2YWx1ZXM9IjA7MTsiIGJlZ2luPSJMM19BLmVuZCIgZHVyPSIwLjEyNXMiIGZpbGw9ImZyZWV6ZSIgLz48YW5pbWF0ZSBhdHRyaWJ1dGVOYW1lPSJvcGFjaXR5IiB2YWx1ZXM9IjE7MDsiIGJlZ2luPSJTeW1ib2xfQS5lbmQrMXM7IiBkdXI9IjAuMTI1cyIgZmlsbD0iZnJlZXplIiAvPjwvcGF0aD48cGF0aCBpZD0iTDMiIGQ9Ik0yNCAyOFYyM0gyNVYyN0gyN1YyOEgyNFoiIGZpbGw9IiMwMzAzMDMiIG9wYWNpdHk9IjAiPjxhbmltYXRlIGlkPSJMM19BIiBhdHRyaWJ1dGVOYW1lPSJvcGFjaXR5IiB2YWx1ZXM9IjA7MTsiIGJlZ2luPSJSMV9BLmVuZCIgZHVyPSIwLjEyNXMiIGZpbGw9ImZyZWV6ZSIgLz48YW5pbWF0ZSBhdHRyaWJ1dGVOYW1lPSJvcGFjaXR5IiB2YWx1ZXM9IjE7MDsiIGJlZ2luPSJTeW1ib2xfQS5lbmQrMXM7IiBkdXI9IjAuMTI1cyIgZmlsbD0iZnJlZXplIiAvPjwvcGF0aD48cGF0aCBpZD0iUjEiIGZpbGwtcnVsZT0iZXZlbm9kZCIgY2xpcC1ydWxlPSJldmVub2RkIiBkPSJNMjIgMjRWMjNIMTlWMjhIMjBWMjdIMjJWMjhIMjNWMjdIMjJWMjZIMjNWMjRIMjJaTTIyIDI0VjI2SDIwVjI0SDIyWiIgZmlsbD0iIzAzMDMwMyIgb3BhY2l0eT0iMCI+PGFuaW1hdGUgaWQ9IlIxX0EiIGF0dHJpYnV0ZU5hbWU9Im9wYWNpdHkiIHZhbHVlcz0iMDsxOyIgYmVnaW49Ik8yX0EuZW5kIiBkdXI9IjAuMTI1cyIgZmlsbD0iZnJlZXplIiAvPjxhbmltYXRlIGF0dHJpYnV0ZU5hbWU9Im9wYWNpdHkiIHZhbHVlcz0iMTswOyIgYmVnaW49IlN5bWJvbF9BLmVuZCsxczsiIGR1cj0iMC4xMjVzIiBmaWxsPSJmcmVlemUiIC8+PC9wYXRoPjxwYXRoIGlkPSJPMiIgZmlsbC1ydWxlPSJldmVub2RkIiBjbGlwLXJ1bGU9ImV2ZW5vZGQiIGQ9Ik0xNCAyNFYyN0gxNVYyOEgxN1YyN0gxOFYyNEgxN1YyM0gxNVYyNEgxNFpNMTUgMjRIMTdWMjdIMTVWMjRaIiBmaWxsPSIjMDMwMzAzIiBvcGFjaXR5PSIwIj48YW5pbWF0ZSBpZD0iTzJfQSIgYXR0cmlidXRlTmFtZT0ib3BhY2l0eSIgdmFsdWVzPSIwOzE7IiBiZWdpbj0iVzFfQS5lbmQiIGR1cj0iMC4xMjVzIiBmaWxsPSJmcmVlemUiIC8+PGFuaW1hdGUgYXR0cmlidXRlTmFtZT0ib3BhY2l0eSIgdmFsdWVzPSIxOzA7IiBiZWdpbj0iU3ltYm9sX0EuZW5kKzFzOyIgZHVyPSIwLjEyNXMiIGZpbGw9ImZyZWV6ZSIgLz48L3BhdGg+PHBhdGggaWQ9IlcxIiBkPSJNOCAyN1YyM0g5VjI3SDEwVjI0SDExVjI3SDEyVjIzSDEzVjI3SDEyVjI4SDExVjI3SDEwVjI4SDlWMjdIOFoiIGZpbGw9IiMwMzAzMDMiIG9wYWNpdHk9IjAiPjxhbmltYXRlIGlkPSJXMV9BIiBhdHRyaWJ1dGVOYW1lPSJvcGFjaXR5IiB2YWx1ZXM9IjA7MTsiIGJlZ2luPSJPMV9BLmVuZCIgZHVyPSIwLjEyNXMiIGZpbGw9ImZyZWV6ZSIgLz48YW5pbWF0ZSBhdHRyaWJ1dGVOYW1lPSJvcGFjaXR5IiB2YWx1ZXM9IjE7MDsiIGJlZ2luPSJTeW1ib2xfQS5lbmQrMXM7IiBkdXI9IjAuMTI1cyIgZmlsbD0iZnJlZXplIiAvPjwvcGF0aD48cGF0aCBpZD0iTzEiIGZpbGwtcnVsZT0iZXZlbm9kZCIgY2xpcC1ydWxlPSJldmVub2RkIiBkPSJNMjUgMTdWMjBIMjZWMjFIMjhWMjBIMjlWMTdIMjhWMTZIMjZWMTdIMjVaTTI2IDE3SDI4VjIwSDI2VjE3WiIgZmlsbD0iIzAzMDMwMyIgb3BhY2l0eT0iMCI+PGFuaW1hdGUgaWQ9Ik8xX0EiIGF0dHJpYnV0ZU5hbWU9Im9wYWNpdHkiIHZhbHVlcz0iMDsxOyIgYmVnaW49IkwyX0EuZW5kIiBkdXI9IjAuMTI1cyIgZmlsbD0iZnJlZXplIiAvPjxhbmltYXRlIGF0dHJpYnV0ZU5hbWU9Im9wYWNpdHkiIHZhbHVlcz0iMTswOyIgYmVnaW49IlN5bWJvbF9BLmVuZCsxczsiIGR1cj0iMC4xMjVzIiBmaWxsPSJmcmVlemUiIC8+PC9wYXRoPjxwYXRoIGlkPSJMMiIgZD0iTTIxIDIxVjE2SDIyVjIwSDI0VjIxSDIxWiIgZmlsbD0iIzAzMDMwMyIgb3BhY2l0eT0iMCI+PGFuaW1hdGUgaWQ9IkwyX0EiIGF0dHJpYnV0ZU5hbWU9Im9wYWNpdHkiIHZhbHVlcz0iMDsxOyIgYmVnaW49IkwxX0EuZW5kIiBkdXI9IjAuMTI1cyIgZmlsbD0iZnJlZXplIiAvPjxhbmltYXRlIGF0dHJpYnV0ZU5hbWU9Im9wYWNpdHkiIHZhbHVlcz0iMTswOyIgYmVnaW49IlN5bWJvbF9BLmVuZCsxczsiIGR1cj0iMC4xMjVzIiBmaWxsPSJmcmVlemUiIC8+PC9wYXRoPjxwYXRoIGlkPSJMMSIgZD0iTTE3IDIxVjE2SDE4VjIwSDIwVjIxSDE3WiIgZmlsbD0iIzAzMDMwMyIgb3BhY2l0eT0iMCI+PGFuaW1hdGUgaWQ9IkwxX0EiIGF0dHJpYnV0ZU5hbWU9Im9wYWNpdHkiIHZhbHVlcz0iMDsxOyIgYmVnaW49IkUxX0EuZW5kIiBkdXI9IjAuMTI1cyIgZmlsbD0iZnJlZXplIiAvPjxhbmltYXRlIGF0dHJpYnV0ZU5hbWU9Im9wYWNpdHkiIHZhbHVlcz0iMTswOyIgYmVnaW49IlN5bWJvbF9BLmVuZCsxczsiIGR1cj0iMC4xMjVzIiBmaWxsPSJmcmVlemUiIC8+PC9wYXRoPjxwYXRoIGlkPSJFMSIgZD0iTTEzIDIxVjE2SDE2VjE3SDE0VjE4SDE2VjE5SDE0VjIwSDE2VjIxSDEzWiIgZmlsbD0iIzAzMDMwMyIgb3BhY2l0eT0iMCI+PGFuaW1hdGUgaWQ9IkUxX0EiIGF0dHJpYnV0ZU5hbWU9Im9wYWNpdHkiIHZhbHVlcz0iMDsxOyIgYmVnaW49IkgxX0EuZW5kIiBkdXI9IjAuMTI1cyIgZmlsbD0iZnJlZXplIiAvPjxhbmltYXRlIGF0dHJpYnV0ZU5hbWU9Im9wYWNpdHkiIHZhbHVlcz0iMTswOyIgYmVnaW49IlN5bWJvbF9BLmVuZCsxczsiIGR1cj0iMC4xMjVzIiBmaWxsPSJmcmVlemUiIC8+PC9wYXRoPjxwYXRoIGlkPSJIMSIgZD0iTTggMjFWMTZIOVYxOEgxMVYxNkgxMlYyMUgxMVYxOUg5VjIxSDhaIiBmaWxsPSIjMDMwMzAzIiBvcGFjaXR5PSIwIj48YW5pbWF0ZSBpZD0iSDFfQSIgYXR0cmlidXRlTmFtZT0ib3BhY2l0eSIgdmFsdWVzPSIwOzE7IiBiZWdpbj0iMHM7SDFfSGlkZV9BLmVuZCswLjEyNXM7IiBkdXI9IjAuMTI1cyIgZmlsbD0iZnJlZXplIiAvPjxhbmltYXRlIGlkPSJIMV9IaWRlX0EiIGF0dHJpYnV0ZU5hbWU9Im9wYWNpdHkiIHZhbHVlcz0iMTswOyIgYmVnaW49IlN5bWJvbF9BLmVuZCsxczsiIGR1cj0iMC4xMjVzIiBmaWxsPSJmcmVlemUiIC8+PC9wYXRoPjwvZz48L2c+PC9nPjwvc3ZnPgo=';

/** Svg width and height which will be used for styling the console message */
const svgSize = 113;

/** Rainbow color palette for used in console */
const rainbowColors = [
  '#F60000',
  '#FF8C00',
  '#FFEE00',
  '#4DE94C',
  '#06D8F6',
  '#3783FF',
  '#882FF6',
];

/** Console message styles */
const consoleStyle = {
  animatedImage: [
    `background-image: url('${base64Svg}')`,
    'background-size: contain',
    'background-position: center center',
    'background-repeat: no-repeat',
    `padding: ${svgSize}px ${svgSize}px`,
    'display: inline-block',
    'font-size: 0',
    'line-height: 1',
    'color: transparent',
  ],
  monospace: [
    'font-family: monospace',
    'line-height: 1.5',
  ],
  boxed: [
    'display: inline-block',
    'border-radius: 0.25em',
    'padding: 0.25em 0.75em',
  ],
  largerFont: [
    'font-size: 16px',
  ],
  mediumFont: [
    'font-size: 14px',
  ],
  smallFont: [
    'font-size: 12px',
  ],
  groupLabel: [
    `background: linear-gradient(
      135deg,
      ${rainbowColors[0]} 0%,
      ${rainbowColors[1]} calc((100 / 7) * 1%),
      ${rainbowColors[2]} calc((100 / 7) * 2%),
      ${rainbowColors[3]} calc((100 / 7) * 3%),
      ${rainbowColors[4]} calc((100 / 7) * 4%),
      ${rainbowColors[5]} calc((100 / 7) * 5%),
      ${rainbowColors[6]} calc((100 / 7) * 6%)
    )`,
    'color: #FFFFFF',
    'font-weight: bold',
    'text-shadow: rgb(0, 0, 0) 2px 0px 0px, rgb(0, 0, 0) 1.75517px 0.958851px 0px, rgb(0, 0, 0) 1.0806px 1.68294px 0px, rgb(0, 0, 0) 0.141474px 1.99499px 0px, rgb(0, 0, 0) -0.832294px 1.81859px 0px, rgb(0, 0, 0) -1.60229px 1.19694px 0px, rgb(0, 0, 0) -1.97998px 0.28224px 0px, rgb(0, 0, 0) -1.87291px -0.701566px 0px, rgb(0, 0, 0) -1.30729px -1.5136px 0px, rgb(0, 0, 0) -0.421592px -1.95506px 0px, rgb(0, 0, 0) 0.567324px -1.91785px 0px, rgb(0, 0, 0) 1.41734px -1.41108px 0px, rgb(0, 0, 0) 1.92034px -0.558831px 0px',
  ],
};

/** Message contents */
const consoleMessage = {
  groupTitle: 'Welcome to my website!',
  message: [
    'I\'m excited to have you here.',
    'Thank you for taking the time to explore my work and learn more about me.',
  ],
};

/** Show welcome message into console */
const displayWelcomeMessage = () => {
  console.info(
    '%c  ',
    consoleStyle.animatedImage.join(';'),
  );
  console.group(
    `%c${consoleMessage.groupTitle}`,
    [
      ...consoleStyle.monospace,
      ...consoleStyle.largerFont,
      ...consoleStyle.boxed,
      ...consoleStyle.groupLabel,
    ].join(';'),
  );
  consoleMessage.message.forEach((output, i) => {
    console.info(
      `%c${i === (consoleMessage.message.length - 1) ? '└' : '├'} %c${output.split('').join('%c')}`,
      [
        ...consoleStyle.monospace,
        ...consoleStyle.mediumFont,
      ].join(';'),
      ...Array.from({ length: output.length }, (_, v) => (
        [
          ...consoleStyle.monospace,
          ...consoleStyle.mediumFont,
          'font-weight: bold',
          'padding: 1px',
          // TODO: find some cool color palette that works on both light and dark theme
          // `color: ${rainbowColors[v % rainbowColors.length]}`,
        ].join(';')
      )),
    );
  });
  console.groupEnd();
};

export default displayWelcomeMessage;
