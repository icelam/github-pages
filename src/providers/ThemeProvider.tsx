import type { FC, PropsWithChildren } from 'react';
import { ThemeProvider as SCThemeProvider } from 'styled-components';
import type { DefaultTheme } from 'styled-components';

/** Any non-theme (light / dark mode ) sensitive styled-components theme variables */
// FIXME: Check if we can remove this provider or even get rid of CSS-in-JS
const theme: DefaultTheme = {};

const ThemeProvider: FC<PropsWithChildren> = ({ children }) => (
  <SCThemeProvider theme={theme}>
    {children}
  </SCThemeProvider>
);

export default ThemeProvider;
