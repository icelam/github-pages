import type { FC, PropsWithChildren } from 'react';
import styled from 'styled-components';
import { MDXProvider as ReactMDXProvider } from '@mdx-js/react';
import { Props as MDXProviderProps } from '@mdx-js/react/lib/index.d';
import { Blockquote, ZoomableImage } from '../components';

const ZoomableImageWithMargin = styled(ZoomableImage)`
  margin-top: var(--var-paragraph-margin);
  margin-bottom: var(--var-paragraph-margin);

  p + & {
    margin-top: var(--var-paragraph-margin);
  }

  &:last-child {
    margin-bottom: 0;
  }
`;

const mdxComponents: MDXProviderProps['components'] = {
  img: ZoomableImageWithMargin,
  blockquote: Blockquote,
};

const MDXProvider: FC<PropsWithChildren> = ({ children }) => (
  <ReactMDXProvider components={mdxComponents}>
    {children}
  </ReactMDXProvider>
);

export default MDXProvider;
