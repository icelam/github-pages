import type { FC } from 'react';
import { HeadProps } from 'gatsby';
import type { MdxPageData } from '../types';

interface PageMeta extends HeadProps {
  options?: {
    description?: string;
    keywords?: string;
    title?: string;
  }
}

const isMdxPageData = (
  data: object,
): data is MdxPageData => data && 'mdx' in data;

const Head: FC<PageMeta> = ({ location: { pathname }, data, options = {} }) => {
  const siteName = process.env.GATSBY_SITE_TITLE;

  const customTitle = isMdxPageData(data)
    ? data.mdx?.frontmatter?.pageTitle
    : options.title;

  const title = customTitle
    ? `${customTitle} | ${process.env.GATSBY_SITE_TITLE}`
    : siteName;

  const description = (
    isMdxPageData(data)
      ? data.mdx?.frontmatter?.pageDescription
      : options.description
  ) || process.env.GATSBY_SITE_DESCRIPTION;

  const keywords = [
    isMdxPageData(data) ? data.mdx?.frontmatter?.pageKeywords : '',
    options.keywords,
    process.env.GATSBY_SITE_KEYWORD,
  ].filter(Boolean).join(', ');

  const url = pathname
    ? process.env.GATSBY_SITE_URL + pathname
    : process.env.GATSBY_SITE_URL;

  return (
    <>
      <title>{title}</title>
      <meta name="title" content={title} />
      <meta name="description" content={description} />
      <meta name="keywords" content={keywords} />
      <meta name="robots" content="index" />
      <meta property="og:type" content="website" />
      <meta property="og:site_name" content={siteName} />
      <meta property="og:title" content={title} />
      <meta property="og:description" content={description} />
      <meta property="og:url" content={url} />
      <meta property="og:image" content={`${process.env.GATSBY_SITE_URL}/images/og-image.png`} />
      <meta property="og:image:secure_url" content={`${process.env.GATSBY_SITE_URL}/images/og-image.png`} />
      <meta property="og:image:type" content="image/png" />
      <meta property="og:image:width" content="1200" />
      <meta property="og:image:height" content="630" />
      <meta property="og:image:alt" content={siteName} />
    </>
  );
};

export default Head;
