import { memo } from 'react';
import styled from 'styled-components';
import { breakpoints } from '../styles';

const Container = styled.div`
  max-width: ${breakpoints.xl}px;
  width: 100%;
  margin: auto;
  padding: 0 5%;
  box-sizing: border-box;
`;

export default memo(Container);
