import type {
  ButtonHTMLAttributes,
} from 'react';
import { memo, forwardRef } from 'react';
import styled from 'styled-components';
import BorderedContainer from './BorderedContainer';

type HTMLButton = ButtonHTMLAttributes<HTMLButtonElement>;

interface ButtonProps extends HTMLButton {
  className?: string;
}

export const PlainButton = styled.button`
  box-shadow: none;
  border: none;
  border-radius: 0;
  margin: 0;
  padding: 0;
  background-color: transparent;
  color: inherit;
  white-space: nowrap;
  font-family: inherit;
  font-size: inherit;
  text-align: center;
  text-decoration: none;

  &, &:active, &:focus {
    outline: none;
  }

  ::-moz-focus-inner {
    border: 0;
  }
`;

const Shadow = styled(PlainButton)`
  display: inline-block;
  transform: translateY(calc(var(--var-pixel-size) * -1));
  filter: drop-shadow(0 var(--var-pixel-size) 0px var(--color-button-border));
  transition: all 150ms;

  &:focus-visible {
    transform: translateY(calc(var(--var-pixel-size) * -2));
    filter: drop-shadow(0 calc(var(--var-pixel-size) * 2) 0px var(--color-button-border));
  }

  &:active {
    transform: unset;
    filter: unset;
  }
`;

const Border = styled(BorderedContainer)`
  --color-card-border: var(--color-button-border);
  --color-card-text: var(--color-button-text);
  --color-card-background: var(--color-button-background);

  @media (pointer: fine) {
    &:hover {
      --color-card-text: var(--color-button-text-hover);
      --color-card-background: var(--color-button-background-hover);
    }
  }

  &:disabled {
    --color-card-text: var(--color-button-text-disabled);
    --color-card-background: var(--color-button-background-disabled);
  }
`;

const Button = forwardRef<HTMLButtonElement, ButtonProps>((props, ref) => {
  const {
    className, children, style, ...attributes
  } = props;

  return (
    <Shadow ref={ref} className={className} style={style} {...attributes}>
      <Border renderAs="span" renderChildAs="span">
        {children}
      </Border>
    </Shadow>
  );
});

export default memo(Button);
