import type { FC } from 'react';
import styled from 'styled-components';
import { motion, AnimatePresence } from 'framer-motion';
import Headroom from 'react-headroom';
import {
  Container, Header, Footer, BackToTop,
} from '.';
import { fadeInOutAnimation } from '../animations';
import { mediaQueries } from '../styles';
import { PageProps } from '../types';

const ContentContainer = styled(Container)`
  padding-top: 1rem;
  padding-bottom: 1rem;

  // header height + footer height
  min-height: calc(100vh - 8.75rem - 5.4375rem);

  @media ${mediaQueries.mobileLandscape} {
    min-height: calc(100vh - 6rem - 5.4375rem);
  }

  @media ${mediaQueries.mobilePortrait} {
    padding-top: 2rem;
    min-height: calc(100vh - 5rem - 5.4375rem);
  }
`;

// Layout - Common page template
const Layout: FC<Pick<PageProps, 'path' | 'children'>> = ({ path, children }) => (
  <>
    <Headroom>
      <Header path={path} />
    </Headroom>
    <ContentContainer>
      <AnimatePresence mode="wait" initial={false}>
        <motion.div
          key={path}
          {...fadeInOutAnimation}
          animate="show"
          exit="hide"
        >
          {children}
        </motion.div>
      </AnimatePresence>
    </ContentContainer>
    <Footer />
    <BackToTop />
  </>
);

export default Layout;
