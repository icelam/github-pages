import type { FC, ComponentProps } from 'react';
import { useRef } from 'react';
import styled from 'styled-components';
import mediumZoom, { Zoom, ZoomOptions } from 'medium-zoom';
import { BorderedContainer } from '.';

const HTMLImage = styled.img`
  width: 100%;
  display: block;
`;

interface ZoomableImageProps extends ComponentProps<'img'> {
  zoomOptions?: ZoomOptions
}

const ZoomableImage: FC<ZoomableImageProps> = ({ className, zoomOptions, ...props }) => {
  const zoomRef = useRef<Zoom | null>(null);

  function getZoom() {
    if (zoomRef.current === null) {
      zoomRef.current = mediumZoom(zoomOptions);
    }

    return zoomRef.current;
  }

  function attachZoom(image: HTMLImageElement | null) {
    const zoom = getZoom();

    if (image) {
      zoom.attach(image);
    } else {
      zoom.detach();
    }
  }

  return (
    <BorderedContainer className={className}>
      <HTMLImage {...props} ref={attachZoom} />
    </BorderedContainer>
  );
};

export default ZoomableImage;
