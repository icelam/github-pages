/* eslint-disable react/destructuring-assignment */
import type { ErrorInfo, ReactNode } from 'react';
import { Component } from 'react';
import { Button, UnexpectedError } from '.';

interface ErrorBoundaryProps {
  children: ReactNode;
}

interface ErrorBoundaryState {
  hasError: boolean;
  error?: Error;
  errorInfo?: ErrorInfo;
}

const initialState = {
  hasError: false,
  error: undefined,
  errorInfo: undefined,
};

class ErrorBoundary extends Component<ErrorBoundaryProps, ErrorBoundaryState> {
  constructor(props: ErrorBoundaryProps) {
    super(props);
    this.state = initialState;
    this.reset = this.reset.bind(this);
  }

  static getDerivedStateFromError(_: Error): ErrorBoundaryState {
    return { hasError: true };
  }

  componentDidCatch(error: Error, errorInfo: ErrorInfo): void {
    // TODO: Integrate Sentry here
    // errorLogger(new Error(`Unhandled error: ${error.message}`, { cause: error as Error }));

    this.setState({ error, errorInfo });
  }

  reset(): void {
    this.setState(initialState);
  }

  render(): ReactNode {
    if (!this.state.hasError) {
      return this.props.children;
    }

    return (
      <UnexpectedError
        errorTitle="Something prevented the preview from rendering."
      >
        {
          process.env.NODE_ENV !== 'production' && this.state.errorInfo?.componentStack
            ? (
              this.state.errorInfo.componentStack
                .replace(/^\s+|\s+$/g, '')
                .split('\n')
                // eslint-disable-next-line react/no-array-index-key
                .map((trace, index) => (<p key={`${trace}-${index}`}>{trace}</p>))
            )
            : this.state.error
              ? (
                <p>
                  Reason:
                  {' '}
                  {this.state.error.message}
                </p>
              )
              : null
        }
        <br />
        <Button onClick={this.reset}>Try Again</Button>
      </UnexpectedError>
    );
  }
}

export default ErrorBoundary;
