import type { FC } from 'react';
import {
  useState, useEffect, useCallback, useMemo, memo,
} from 'react';
import styled, { css } from 'styled-components';
import { AvailableThemes } from '../types';
import { mediaQueries } from '../styles';
import { Spinner } from '.';
import { ReactComponent as SunIcon } from '../assets/images/icons/sun.svg';
import { ReactComponent as MoonIcon } from '../assets/images/icons/moon.svg';
import { PlainButton } from './Button';

const themeIconStyle = css`
  font-size: inherit;
  font-family: inherit;
  color: var(--color-navigation-link);
  padding: 0.125rem;
  box-sizing: content-box;

  &, > svg {
    display: block;
    width: 1.125rem;
    height: 1.125rem;
  }

  @media ${mediaQueries.tabletDesktop} {
    padding: 0.1875rem;

    &, > svg {
      width: 1.375rem;
      height: 1.375rem;
    }
  }
`;

const StyledButton = styled(PlainButton)`
  ${themeIconStyle}

  @media (pointer: fine) {
    &:hover {
      color: var(--color-navigation-link-hover);
    }
  }

  &:focus-visible {
    outline: var(--var-pixel-size) solid currentColor;
  }
`;

const ThemeLoadingSpinner = styled(Spinner)`
  ${themeIconStyle}
`;

const ThemeToggle: FC = () => {
  const [currentTheme, setCurrentTheme] = useState<AvailableThemes | null>(null);
  const isDarkTheme = useMemo(() => currentTheme === 'dark', [currentTheme]);

  useEffect(() => {
    setCurrentTheme(window.__GATSBY_THEME__);
  }, []);

  const onClick = useCallback(() => {
    const newTheme = isDarkTheme ? 'light' : 'dark';
    window.__toggleTheme(newTheme);
    setCurrentTheme(newTheme);
  }, [isDarkTheme]);

  // Render a placeholder on server-side rendering as we can't determine the correct initial state
  if (!currentTheme) {
    return (
      <ThemeLoadingSpinner />
    );
  }

  return (
    <StyledButton onClick={onClick} aria-label={`Switch to ${isDarkTheme ? 'Light' : 'Dark'} Mode`}>
      {isDarkTheme ? <SunIcon /> : <MoonIcon />}
    </StyledButton>
  );
};

export default memo(ThemeToggle);
