import type { FC, PropsWithChildren } from 'react';
import styled from 'styled-components';
import { mediaQueries } from '../styles';

const FullPageDiv = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-start;
  box-sizing: border-box;
  // header height + footer height + container padding
  min-height: calc(100vh - 8.75rem - 5.4375rem - 2rem);

  @media ${mediaQueries.mobileLandscape} {
    min-height: calc(100vh - 6rem - 5.4375rem - 2rem);
  }

  @media ${mediaQueries.mobilePortrait} {
    min-height: calc(100vh - 5rem - 5.4375rem - 3rem);
  }
`;

const ErrorCode = styled.h1`
  font-size: 3.75rem;
  line-height: 1;
  margin: 0 0 1.875rem 0;
  font-weight: normal;
`;

const ErrorTitle = styled.h2`
  font-size: 1.6875rem;
  margin: 0;
  font-weight: normal;
`;

const ErrorDescription = styled.div`
  font-size: 1.125rem;
`;

interface UnexpectedErrorProps {
  errorCode?: string;
  errorTitle: string;
}

const UnexpectedError: FC<PropsWithChildren<UnexpectedErrorProps>> = ({
  errorCode,
  errorTitle,
  children,
}) => (
  <FullPageDiv>
    <ErrorCode>{errorCode || 'Oops!'}</ErrorCode>
    <ErrorTitle>{errorTitle}</ErrorTitle>
    <ErrorDescription>
      {children}
    </ErrorDescription>
  </FullPageDiv>
);

export default UnexpectedError;
