import styled, { keyframes } from 'styled-components';

const spin = keyframes`
  0% {
    box-shadow:
      calc(var(--var-pixel-size) * 0) calc(var(--var-pixel-size) * -3) currentColor,
      calc(var(--var-pixel-size) * 1) calc(var(--var-pixel-size) * -3) currentColor,
      calc(var(--var-pixel-size) * 2) calc(var(--var-pixel-size) * -2) currentColor,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * -1) currentColor,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * 0) currentColor,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * 1) currentColor,
      calc(var(--var-pixel-size) * 2) calc(var(--var-pixel-size) * 2) currentColor,
      calc(var(--var-pixel-size) * 1) calc(var(--var-pixel-size) * 3) currentColor,
      calc(var(--var-pixel-size) * 0) calc(var(--var-pixel-size) * 3) transparent,
      calc(var(--var-pixel-size) * -1) calc(var(--var-pixel-size) * 3) transparent,
      calc(var(--var-pixel-size) * -2) calc(var(--var-pixel-size) * 2) transparent,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * 1) transparent,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * 0) transparent,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * -1) transparent,
      calc(var(--var-pixel-size) * -2) calc(var(--var-pixel-size) * -2) transparent,
      calc(var(--var-pixel-size) * -1) calc(var(--var-pixel-size) * -3) transparent;
  }
  6.25% {
    box-shadow:
      calc(var(--var-pixel-size) * 0) calc(var(--var-pixel-size) * -3) transparent,
      calc(var(--var-pixel-size) * 1) calc(var(--var-pixel-size) * -3) currentColor,
      calc(var(--var-pixel-size) * 2) calc(var(--var-pixel-size) * -2) currentColor,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * -1) currentColor,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * 0) currentColor,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * 1) currentColor,
      calc(var(--var-pixel-size) * 2) calc(var(--var-pixel-size) * 2) currentColor,
      calc(var(--var-pixel-size) * 1) calc(var(--var-pixel-size) * 3) currentColor,
      calc(var(--var-pixel-size) * 0) calc(var(--var-pixel-size) * 3) currentColor,
      calc(var(--var-pixel-size) * -1) calc(var(--var-pixel-size) * 3) transparent,
      calc(var(--var-pixel-size) * -2) calc(var(--var-pixel-size) * 2) transparent,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * 1) transparent,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * 0) transparent,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * -1) transparent,
      calc(var(--var-pixel-size) * -2) calc(var(--var-pixel-size) * -2) transparent,
      calc(var(--var-pixel-size) * -1) calc(var(--var-pixel-size) * -3) transparent;
  }
  12.5% {
    box-shadow:
      calc(var(--var-pixel-size) * 0) calc(var(--var-pixel-size) * -3) transparent,
      calc(var(--var-pixel-size) * 1) calc(var(--var-pixel-size) * -3) transparent,
      calc(var(--var-pixel-size) * 2) calc(var(--var-pixel-size) * -2) currentColor,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * -1) currentColor,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * 0) currentColor,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * 1) currentColor,
      calc(var(--var-pixel-size) * 2) calc(var(--var-pixel-size) * 2) currentColor,
      calc(var(--var-pixel-size) * 1) calc(var(--var-pixel-size) * 3) currentColor,
      calc(var(--var-pixel-size) * 0) calc(var(--var-pixel-size) * 3) currentColor,
      calc(var(--var-pixel-size) * -1) calc(var(--var-pixel-size) * 3) currentColor,
      calc(var(--var-pixel-size) * -2) calc(var(--var-pixel-size) * 2) transparent,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * 1) transparent,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * 0) transparent,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * -1) transparent,
      calc(var(--var-pixel-size) * -2) calc(var(--var-pixel-size) * -2) transparent,
      calc(var(--var-pixel-size) * -1) calc(var(--var-pixel-size) * -3) transparent;
  }
  18.75% {
    box-shadow:
      calc(var(--var-pixel-size) * 0) calc(var(--var-pixel-size) * -3) transparent,
      calc(var(--var-pixel-size) * 1) calc(var(--var-pixel-size) * -3) transparent,
      calc(var(--var-pixel-size) * 2) calc(var(--var-pixel-size) * -2) transparent,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * -1) currentColor,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * 0) currentColor,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * 1) currentColor,
      calc(var(--var-pixel-size) * 2) calc(var(--var-pixel-size) * 2) currentColor,
      calc(var(--var-pixel-size) * 1) calc(var(--var-pixel-size) * 3) currentColor,
      calc(var(--var-pixel-size) * 0) calc(var(--var-pixel-size) * 3) currentColor,
      calc(var(--var-pixel-size) * -1) calc(var(--var-pixel-size) * 3) currentColor,
      calc(var(--var-pixel-size) * -2) calc(var(--var-pixel-size) * 2) currentColor,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * 1) transparent,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * 0) transparent,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * -1) transparent,
      calc(var(--var-pixel-size) * -2) calc(var(--var-pixel-size) * -2) transparent,
      calc(var(--var-pixel-size) * -1) calc(var(--var-pixel-size) * -3) transparent;
  }
  25% {
    box-shadow:
      calc(var(--var-pixel-size) * 0) calc(var(--var-pixel-size) * -3) transparent,
      calc(var(--var-pixel-size) * 1) calc(var(--var-pixel-size) * -3) transparent,
      calc(var(--var-pixel-size) * 2) calc(var(--var-pixel-size) * -2) transparent,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * -1) transparent,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * 0) currentColor,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * 1) currentColor,
      calc(var(--var-pixel-size) * 2) calc(var(--var-pixel-size) * 2) currentColor,
      calc(var(--var-pixel-size) * 1) calc(var(--var-pixel-size) * 3) currentColor,
      calc(var(--var-pixel-size) * 0) calc(var(--var-pixel-size) * 3) currentColor,
      calc(var(--var-pixel-size) * -1) calc(var(--var-pixel-size) * 3) currentColor,
      calc(var(--var-pixel-size) * -2) calc(var(--var-pixel-size) * 2) currentColor,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * 1) currentColor,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * 0) transparent,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * -1) transparent,
      calc(var(--var-pixel-size) * -2) calc(var(--var-pixel-size) * -2) transparent,
      calc(var(--var-pixel-size) * -1) calc(var(--var-pixel-size) * -3) transparent;
  }
  31.25% {
    box-shadow:
      calc(var(--var-pixel-size) * 0) calc(var(--var-pixel-size) * -3) transparent,
      calc(var(--var-pixel-size) * 1) calc(var(--var-pixel-size) * -3) transparent,
      calc(var(--var-pixel-size) * 2) calc(var(--var-pixel-size) * -2) transparent,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * -1) transparent,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * 0) transparent,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * 1) currentColor,
      calc(var(--var-pixel-size) * 2) calc(var(--var-pixel-size) * 2) currentColor,
      calc(var(--var-pixel-size) * 1) calc(var(--var-pixel-size) * 3) currentColor,
      calc(var(--var-pixel-size) * 0) calc(var(--var-pixel-size) * 3) currentColor,
      calc(var(--var-pixel-size) * -1) calc(var(--var-pixel-size) * 3) currentColor,
      calc(var(--var-pixel-size) * -2) calc(var(--var-pixel-size) * 2) currentColor,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * 1) currentColor,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * 0) currentColor,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * -1) transparent,
      calc(var(--var-pixel-size) * -2) calc(var(--var-pixel-size) * -2) transparent,
      calc(var(--var-pixel-size) * -1) calc(var(--var-pixel-size) * -3) transparent;
  }
  37.5% {
    box-shadow:
      calc(var(--var-pixel-size) * 0) calc(var(--var-pixel-size) * -3) transparent,
      calc(var(--var-pixel-size) * 1) calc(var(--var-pixel-size) * -3) transparent,
      calc(var(--var-pixel-size) * 2) calc(var(--var-pixel-size) * -2) transparent,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * -1) transparent,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * 0) transparent,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * 1) transparent,
      calc(var(--var-pixel-size) * 2) calc(var(--var-pixel-size) * 2) currentColor,
      calc(var(--var-pixel-size) * 1) calc(var(--var-pixel-size) * 3) currentColor,
      calc(var(--var-pixel-size) * 0) calc(var(--var-pixel-size) * 3) currentColor,
      calc(var(--var-pixel-size) * -1) calc(var(--var-pixel-size) * 3) currentColor,
      calc(var(--var-pixel-size) * -2) calc(var(--var-pixel-size) * 2) currentColor,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * 1) currentColor,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * 0) currentColor,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * -1) currentColor,
      calc(var(--var-pixel-size) * -2) calc(var(--var-pixel-size) * -2) transparent,
      calc(var(--var-pixel-size) * -1) calc(var(--var-pixel-size) * -3) transparent;
  }
  43.75% {
    box-shadow:
      calc(var(--var-pixel-size) * 0) calc(var(--var-pixel-size) * -3) transparent,
      calc(var(--var-pixel-size) * 1) calc(var(--var-pixel-size) * -3) transparent,
      calc(var(--var-pixel-size) * 2) calc(var(--var-pixel-size) * -2) transparent,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * -1) transparent,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * 0) transparent,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * 1) transparent,
      calc(var(--var-pixel-size) * 2) calc(var(--var-pixel-size) * 2) transparent,
      calc(var(--var-pixel-size) * 1) calc(var(--var-pixel-size) * 3) currentColor,
      calc(var(--var-pixel-size) * 0) calc(var(--var-pixel-size) * 3) currentColor,
      calc(var(--var-pixel-size) * -1) calc(var(--var-pixel-size) * 3) currentColor,
      calc(var(--var-pixel-size) * -2) calc(var(--var-pixel-size) * 2) currentColor,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * 1) currentColor,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * 0) currentColor,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * -1) currentColor,
      calc(var(--var-pixel-size) * -2) calc(var(--var-pixel-size) * -2) currentColor,
      calc(var(--var-pixel-size) * -1) calc(var(--var-pixel-size) * -3) transparent;
  }
  50% {
    box-shadow:
      calc(var(--var-pixel-size) * 0) calc(var(--var-pixel-size) * -3) transparent,
      calc(var(--var-pixel-size) * 1) calc(var(--var-pixel-size) * -3) transparent,
      calc(var(--var-pixel-size) * 2) calc(var(--var-pixel-size) * -2) transparent,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * -1) transparent,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * 0) transparent,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * 1) transparent,
      calc(var(--var-pixel-size) * 2) calc(var(--var-pixel-size) * 2) transparent,
      calc(var(--var-pixel-size) * 1) calc(var(--var-pixel-size) * 3) transparent,
      calc(var(--var-pixel-size) * 0) calc(var(--var-pixel-size) * 3) currentColor,
      calc(var(--var-pixel-size) * -1) calc(var(--var-pixel-size) * 3) currentColor,
      calc(var(--var-pixel-size) * -2) calc(var(--var-pixel-size) * 2) currentColor,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * 1) currentColor,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * 0) currentColor,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * -1) currentColor,
      calc(var(--var-pixel-size) * -2) calc(var(--var-pixel-size) * -2) currentColor,
      calc(var(--var-pixel-size) * -1) calc(var(--var-pixel-size) * -3) currentColor;
  }
  56.25% {
    box-shadow:
      calc(var(--var-pixel-size) * 0) calc(var(--var-pixel-size) * -3) currentColor,
      calc(var(--var-pixel-size) * 1) calc(var(--var-pixel-size) * -3) transparent,
      calc(var(--var-pixel-size) * 2) calc(var(--var-pixel-size) * -2) transparent,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * -1) transparent,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * 0) transparent,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * 1) transparent,
      calc(var(--var-pixel-size) * 2) calc(var(--var-pixel-size) * 2) transparent,
      calc(var(--var-pixel-size) * 1) calc(var(--var-pixel-size) * 3) transparent,
      calc(var(--var-pixel-size) * 0) calc(var(--var-pixel-size) * 3) transparent,
      calc(var(--var-pixel-size) * -1) calc(var(--var-pixel-size) * 3) currentColor,
      calc(var(--var-pixel-size) * -2) calc(var(--var-pixel-size) * 2) currentColor,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * 1) currentColor,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * 0) currentColor,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * -1) currentColor,
      calc(var(--var-pixel-size) * -2) calc(var(--var-pixel-size) * -2) currentColor,
      calc(var(--var-pixel-size) * -1) calc(var(--var-pixel-size) * -3) currentColor;
  }
  62.5% {
    box-shadow:
      calc(var(--var-pixel-size) * 0) calc(var(--var-pixel-size) * -3) currentColor,
      calc(var(--var-pixel-size) * 1) calc(var(--var-pixel-size) * -3) currentColor,
      calc(var(--var-pixel-size) * 2) calc(var(--var-pixel-size) * -2) transparent,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * -1) transparent,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * 0) transparent,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * 1) transparent,
      calc(var(--var-pixel-size) * 2) calc(var(--var-pixel-size) * 2) transparent,
      calc(var(--var-pixel-size) * 1) calc(var(--var-pixel-size) * 3) transparent,
      calc(var(--var-pixel-size) * 0) calc(var(--var-pixel-size) * 3) transparent,
      calc(var(--var-pixel-size) * -1) calc(var(--var-pixel-size) * 3) transparent,
      calc(var(--var-pixel-size) * -2) calc(var(--var-pixel-size) * 2) currentColor,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * 1) currentColor,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * 0) currentColor,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * -1) currentColor,
      calc(var(--var-pixel-size) * -2) calc(var(--var-pixel-size) * -2) currentColor,
      calc(var(--var-pixel-size) * -1) calc(var(--var-pixel-size) * -3) currentColor;
  }
  68.75% {
    box-shadow:
      calc(var(--var-pixel-size) * 0) calc(var(--var-pixel-size) * -3) currentColor,
      calc(var(--var-pixel-size) * 1) calc(var(--var-pixel-size) * -3) currentColor,
      calc(var(--var-pixel-size) * 2) calc(var(--var-pixel-size) * -2) currentColor,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * -1) transparent,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * 0) transparent,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * 1) transparent,
      calc(var(--var-pixel-size) * 2) calc(var(--var-pixel-size) * 2) transparent,
      calc(var(--var-pixel-size) * 1) calc(var(--var-pixel-size) * 3) transparent,
      calc(var(--var-pixel-size) * 0) calc(var(--var-pixel-size) * 3) transparent,
      calc(var(--var-pixel-size) * -1) calc(var(--var-pixel-size) * 3) transparent,
      calc(var(--var-pixel-size) * -2) calc(var(--var-pixel-size) * 2) transparent,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * 1) currentColor,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * 0) currentColor,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * -1) currentColor,
      calc(var(--var-pixel-size) * -2) calc(var(--var-pixel-size) * -2) currentColor,
      calc(var(--var-pixel-size) * -1) calc(var(--var-pixel-size) * -3) currentColor;
  }
  75% {
    box-shadow:
      calc(var(--var-pixel-size) * 0) calc(var(--var-pixel-size) * -3) currentColor,
      calc(var(--var-pixel-size) * 1) calc(var(--var-pixel-size) * -3) currentColor,
      calc(var(--var-pixel-size) * 2) calc(var(--var-pixel-size) * -2) currentColor,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * -1) currentColor,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * 0) transparent,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * 1) transparent,
      calc(var(--var-pixel-size) * 2) calc(var(--var-pixel-size) * 2) transparent,
      calc(var(--var-pixel-size) * 1) calc(var(--var-pixel-size) * 3) transparent,
      calc(var(--var-pixel-size) * 0) calc(var(--var-pixel-size) * 3) transparent,
      calc(var(--var-pixel-size) * -1) calc(var(--var-pixel-size) * 3) transparent,
      calc(var(--var-pixel-size) * -2) calc(var(--var-pixel-size) * 2) transparent,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * 1) transparent,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * 0) currentColor,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * -1) currentColor,
      calc(var(--var-pixel-size) * -2) calc(var(--var-pixel-size) * -2) currentColor,
      calc(var(--var-pixel-size) * -1) calc(var(--var-pixel-size) * -3) currentColor;
  }
  81.25% {
    box-shadow:
      calc(var(--var-pixel-size) * 0) calc(var(--var-pixel-size) * -3) currentColor,
      calc(var(--var-pixel-size) * 1) calc(var(--var-pixel-size) * -3) currentColor,
      calc(var(--var-pixel-size) * 2) calc(var(--var-pixel-size) * -2) currentColor,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * -1) currentColor,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * 0) currentColor,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * 1) transparent,
      calc(var(--var-pixel-size) * 2) calc(var(--var-pixel-size) * 2) transparent,
      calc(var(--var-pixel-size) * 1) calc(var(--var-pixel-size) * 3) transparent,
      calc(var(--var-pixel-size) * 0) calc(var(--var-pixel-size) * 3) transparent,
      calc(var(--var-pixel-size) * -1) calc(var(--var-pixel-size) * 3) transparent,
      calc(var(--var-pixel-size) * -2) calc(var(--var-pixel-size) * 2) transparent,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * 1) transparent,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * 0) transparent,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * -1) currentColor,
      calc(var(--var-pixel-size) * -2) calc(var(--var-pixel-size) * -2) currentColor,
      calc(var(--var-pixel-size) * -1) calc(var(--var-pixel-size) * -3) currentColor;
  }
  87.5% {
    box-shadow:
      calc(var(--var-pixel-size) * 0) calc(var(--var-pixel-size) * -3) currentColor,
      calc(var(--var-pixel-size) * 1) calc(var(--var-pixel-size) * -3) currentColor,
      calc(var(--var-pixel-size) * 2) calc(var(--var-pixel-size) * -2) currentColor,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * -1) currentColor,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * 0) currentColor,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * 1) currentColor,
      calc(var(--var-pixel-size) * 2) calc(var(--var-pixel-size) * 2) transparent,
      calc(var(--var-pixel-size) * 1) calc(var(--var-pixel-size) * 3) transparent,
      calc(var(--var-pixel-size) * 0) calc(var(--var-pixel-size) * 3) transparent,
      calc(var(--var-pixel-size) * -1) calc(var(--var-pixel-size) * 3) transparent,
      calc(var(--var-pixel-size) * -2) calc(var(--var-pixel-size) * 2) transparent,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * 1) transparent,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * 0) transparent,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * -1) transparent,
      calc(var(--var-pixel-size) * -2) calc(var(--var-pixel-size) * -2) currentColor,
      calc(var(--var-pixel-size) * -1) calc(var(--var-pixel-size) * -3) currentColor;
  }
  93.75% {
    box-shadow:
      calc(var(--var-pixel-size) * 0) calc(var(--var-pixel-size) * -3) currentColor,
      calc(var(--var-pixel-size) * 1) calc(var(--var-pixel-size) * -3) currentColor,
      calc(var(--var-pixel-size) * 2) calc(var(--var-pixel-size) * -2) currentColor,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * -1) currentColor,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * 0) currentColor,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * 1) currentColor,
      calc(var(--var-pixel-size) * 2) calc(var(--var-pixel-size) * 2) currentColor,
      calc(var(--var-pixel-size) * 1) calc(var(--var-pixel-size) * 3) transparent,
      calc(var(--var-pixel-size) * 0) calc(var(--var-pixel-size) * 3) transparent,
      calc(var(--var-pixel-size) * -1) calc(var(--var-pixel-size) * 3) transparent,
      calc(var(--var-pixel-size) * -2) calc(var(--var-pixel-size) * 2) transparent,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * 1) transparent,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * 0) transparent,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * -1) transparent,
      calc(var(--var-pixel-size) * -2) calc(var(--var-pixel-size) * -2) transparent,
      calc(var(--var-pixel-size) * -1) calc(var(--var-pixel-size) * -3) currentColor;
  }
  100% {
    box-shadow:
      calc(var(--var-pixel-size) * 0) calc(var(--var-pixel-size) * -3) currentColor,
      calc(var(--var-pixel-size) * 1) calc(var(--var-pixel-size) * -3) currentColor,
      calc(var(--var-pixel-size) * 2) calc(var(--var-pixel-size) * -2) currentColor,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * -1) currentColor,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * 0) currentColor,
      calc(var(--var-pixel-size) * 3) calc(var(--var-pixel-size) * 1) currentColor,
      calc(var(--var-pixel-size) * 2) calc(var(--var-pixel-size) * 2) currentColor,
      calc(var(--var-pixel-size) * 1) calc(var(--var-pixel-size) * 3) currentColor,
      calc(var(--var-pixel-size) * 0) calc(var(--var-pixel-size) * 3) transparent,
      calc(var(--var-pixel-size) * -1) calc(var(--var-pixel-size) * 3) transparent,
      calc(var(--var-pixel-size) * -2) calc(var(--var-pixel-size) * 2) transparent,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * 1) transparent,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * 0) transparent,
      calc(var(--var-pixel-size) * -3) calc(var(--var-pixel-size) * -1) transparent,
      calc(var(--var-pixel-size) * -2) calc(var(--var-pixel-size) * -2) transparent,
      calc(var(--var-pixel-size) * -1) calc(var(--var-pixel-size) * -3) transparent;
  }
`;

const Spinner = styled.div`
  height: 100%;
  width: 100%;
  position: relative;

  &:after{
    content: '';
    animation: ${spin} 0.5s linear infinite;
    height: var(--var-pixel-size);
    width: var(--var-pixel-size);
    position: absolute;
    top: 50%;
    left: 50%;
    margin: calc(var(--var-pixel-size) / 2 * -1);
  }
`;

export default Spinner;
