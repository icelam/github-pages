import type { FC, PropsWithChildren } from 'react';
import { memo } from 'react';
import styled from 'styled-components';
import BorderedContainer, { BorderedContent } from './BorderedContainer';

interface TagProps {
  $backgroundColor: string;
  $textColor: string;
}

const TagContainer = styled(BorderedContainer)<TagProps>`
  --color-card-border: ${(props) => props.$backgroundColor};
  --color-card-background: ${(props) => props.$backgroundColor};
  --color-card-text: ${(props) => props.$textColor};
  margin: 0.25rem 0.25rem 0.25rem 0;
  display: inline-block;
  font-size: 0.875rem;
  line-height: 1.25rem;
  vertical-align: middle;

  ${BorderedContent} {
    padding: 0 0.25rem;
  }
`;

const Tag: FC<PropsWithChildren<TagProps>> = (
  { children, $backgroundColor, $textColor },
) => (
  <TagContainer
    $backgroundColor={$backgroundColor}
    $textColor={$textColor}
  >
    {children}
  </TagContainer>
);

export default memo(Tag);
