import type { FC } from 'react';
import { memo } from 'react';
import styled from 'styled-components';

import Container from './Container';

const FooterWrapper = styled.div`
  padding: 2.375rem 0 1.75rem 0;
  font-size: 0.875rem;
  line-height: 1.3125rem;
  color: var(--color-secondary-text);
`;

const Footer: FC = () => {
  const year = new Date().getFullYear();

  return (
    <FooterWrapper>
      <Container>
        &copy;
        {' '}
        <span suppressHydrationWarning>
          {year}
        </span>
        {' '}
        Ice Lam
      </Container>
    </FooterWrapper>
  );
};

export default memo(Footer);
