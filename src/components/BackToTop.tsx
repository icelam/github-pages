import type { FC } from 'react';
import { memo } from 'react';
import styled from 'styled-components';
import {
  motion, useScroll, useMotionValueEvent, useAnimationControls,
} from 'framer-motion';
import Button from './Button';
import { ReactComponent as ArrowUpIcon } from '../assets/images/icons/top.svg';
import { fadeInOutAnimation } from '../animations';
import { isBrowser } from '../utils';
import { BorderedContent } from './BorderedContainer';

const StyledButton = styled(Button)`
  position: fixed;
  bottom: 1.125rem;
  right: calc(var(--var-pixel-size) * -2);
  font-size: 0.875em;
  line-height: 1rem;

  ${BorderedContent} {
    padding: 0.375rem 0.625rem;
  }
`;

const ButtonIcon = styled(ArrowUpIcon)`
  display: block;
  width: 0.875em;
  height: auto;
  margin: 0.1875rem auto 0.3125rem auto;
`;

const threshold = 300;

const scrollToTop = () => {
  if (!isBrowser()) {
    return;
  }

  window.scrollTo({ top: 0, behavior: 'smooth' });
};

const MotionButton = motion(StyledButton);

const BackToTop: FC = () => {
  const { scrollY } = useScroll();
  const controls = useAnimationControls();

  useMotionValueEvent(scrollY, 'change', (y) => {
    if (y > threshold) {
      controls.set('show');
    } else {
      controls.set('hide');
    }
  });

  return (
    <MotionButton
      {...fadeInOutAnimation}
      animate={controls}
      key="backToTopButton"
      onClick={scrollToTop}
    >
      <ButtonIcon />
      Top
    </MotionButton>
  );
};

export default memo(BackToTop);
