/* eslint-disable object-curly-newline */
import { memo } from 'react';
import styled from 'styled-components';
import { mediaQueries } from '../styles';

interface FlexRowProps {
  $justifyContent?: string;
  $alignItems?: string;
}

interface FlexColumnProps {
  $xs?: number;
  $sm?: number;
  $md?: number;
  $lg?: number;
}

const calcPercent = (span?: number): number | undefined => (span ? (span / 12) * 100 : undefined);

const Row = styled.div<FlexRowProps>`
  box-sizing: border-box;
  display: flex;
  flex: 0 1 auto;
  flex-direction: row;
  flex-wrap: wrap;
  margin-right: calc(var(--var-flex-gap-left-right) * -1);
  margin-left: calc(var(--var-flex-gap-left-right) * -1);
  margin-bottom: calc(var(--var-flex-gap-bottom) * -1);
  justifyContent: ${({ $justifyContent }) => $justifyContent};
  align-items: ${({ $alignItems }) => $alignItems};
`;

const Column = styled.div<FlexColumnProps>`
  box-sizing: border-box;
  padding-right: var(--var-flex-gap-left-right);
  padding-left: var(--var-flex-gap-left-right);
  margin-bottom: var(--var-flex-gap-bottom);
  flex: 0 1 ${({ $xs: xs }) => calcPercent(xs) || 100}%;
  max-width: ${({ $xs: xs }) => calcPercent(xs) || 100}%;

  @media ${mediaQueries.mobileLandscape} {
    flex: 0 1 ${({ $sm: sm, $xs: xs }) => calcPercent(sm) || calcPercent(xs) || 100}%;
    max-width: ${({ $sm: sm, $xs: xs }) => calcPercent(sm) || calcPercent(xs) || 100}%;
  }

  @media ${mediaQueries.tablet} {
    flex: 0 1 ${({ $md: md, $sm: sm, $xs: xs }) => calcPercent(md) || calcPercent(sm) || calcPercent(xs) || 100}%;
    max-width: ${({ $md: md, $sm: sm, $xs: xs }) => calcPercent(md) || calcPercent(sm) || calcPercent(xs) || 100}%;
  }

  @media ${mediaQueries.desktop} {
    flex: 0 1 ${({ $lg: lg, $md: md, $sm: sm, $xs: xs }) => calcPercent(lg) || calcPercent(md) || calcPercent(sm) || calcPercent(xs) || 100}%;
    max-width: ${({ $lg: lg, $md: md, $sm: sm, $xs: xs }) => calcPercent(lg) || calcPercent(md) || calcPercent(sm) || calcPercent(xs) || 100}%;
  }
`;

export const FlexRow = memo(Row);
export const FlexColumn = memo(Column);
