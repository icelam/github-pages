import type { FC, PropsWithChildren } from 'react';
import { memo } from 'react';
import styled from 'styled-components';
import type { IGatsbyImageData } from 'gatsby-plugin-image';
import { GatsbyImage } from 'gatsby-plugin-image';
import { BorderedContainer } from '.';

interface CardProps {
  image: IGatsbyImageData;
  imageAlt: string;
  imageLazyLoad: boolean;
  className?: string;
}

const CardWrapper = styled(BorderedContainer)`
  height: 100%;
  display: flex;
`;

const CardImage = styled(GatsbyImage)`
  width: 100%;
  display: block;
  border-bottom: var(--var-pixel-size) solid var(--color-card-border);
`;

const CardContent = styled.div`
  padding: 0.625rem 0.9375rem;
`;

const Card: FC<PropsWithChildren<CardProps>> = ({
  image,
  imageAlt,
  imageLazyLoad,
  children,
  className,
}) => (
  <CardWrapper className={className}>
    <CardImage image={image} alt={imageAlt} loading={imageLazyLoad ? 'lazy' : undefined} />
    <CardContent>
      {children}
    </CardContent>
  </CardWrapper>
);

export default memo(Card);
