import type { FC, PropsWithChildren } from 'react';
import { memo } from 'react';
import styled from 'styled-components';
import BorderedContainer, { BorderedContent } from './BorderedContainer';

interface BlockquoteProps {
  className?: string;
}

const StyledBorderedContainer = styled(BorderedContainer)`
  --color-card-border: var(--color-blockquote-border);
  --color-card-text: var(--color-blockquote-text);
  --color-card-background: var(--color-blockquote-background);
  margin: calc(var(--var-paragraph-margin)*1.5) 0;

  ${BorderedContent} {
    padding: 1rem 1.25rem;
    font-size: 0.875rem;
    line-height: 1.5;
  }

  p {
    margin: 0;
  }

  p + p {
    margin-top: 1em;
  }
`;

const Blockquote: FC<PropsWithChildren<BlockquoteProps>> = ({
  children,
  className,
}) => (
  <StyledBorderedContainer className={className} renderAs="blockquote">
    {children}
  </StyledBorderedContainer>
);

export default memo(Blockquote);
