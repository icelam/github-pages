import type { FC, PropsWithChildren } from 'react';
import { memo } from 'react';
import styled from 'styled-components';

interface BorderedContainerProps {
  className?: string;
  /**
   * Polymorphic prop of styled component for container.
   * Reference: https://styled-components.com/docs/api#as-polymorphic-prop
   */
  renderAs?: string;
  /**
   * Polymorphic prop of styled component for content wrapper.
   * Reference: https://styled-components.com/docs/api#as-polymorphic-prop
   */
  renderChildAs?: string;
}

const Border = styled.div`
  display: block;
  box-sizing: border-box;
  transition: var(--var-theme-toggle-transition);
  padding: var(--var-pixel-size);
  background-color: var(--color-card-border);
  clip-path: polygon(0 calc(var(--var-pixel-size) * 2), var(--var-pixel-size) calc(var(--var-pixel-size) * 2), var(--var-pixel-size) var(--var-pixel-size), calc(var(--var-pixel-size) * 2) var(--var-pixel-size), calc(var(--var-pixel-size) * 2) 0, calc(100% - calc(var(--var-pixel-size) * 2)) 0, calc(100% - calc(var(--var-pixel-size) * 2)) var(--var-pixel-size), calc(100% - var(--var-pixel-size)) var(--var-pixel-size), calc(100% - var(--var-pixel-size)) calc(var(--var-pixel-size) * 2), 100% calc(var(--var-pixel-size) * 2), 100% calc(100% - calc(var(--var-pixel-size) * 2)), calc(100% - var(--var-pixel-size)) calc(100% - calc(var(--var-pixel-size) * 2)), calc(100% - var(--var-pixel-size)) calc(100% - var(--var-pixel-size)), calc(100% - calc(var(--var-pixel-size) * 2)) calc(100% - var(--var-pixel-size)), calc(100% - calc(var(--var-pixel-size) * 2)) 100%, calc(var(--var-pixel-size) * 2) 100%, calc(var(--var-pixel-size) * 2) calc(100% - var(--var-pixel-size)), var(--var-pixel-size) calc(100% - var(--var-pixel-size)), var(--var-pixel-size) calc(100% - calc(var(--var-pixel-size) * 2)), 0 calc(100% - calc(var(--var-pixel-size) * 2)));
`;

export const BorderedContent = styled.div`
  display: block;
  box-sizing: border-box;
  width: 100%;
  background-color: var(--color-card-background);
  color: var(--color-card-text);
  transition: var(--var-theme-toggle-transition);
  clip-path: polygon(var(--var-pixel-size) 0, calc(100% - var(--var-pixel-size)) 0, calc(100% - var(--var-pixel-size)) var(--var-pixel-size), 100% var(--var-pixel-size), 100% calc(100% - var(--var-pixel-size)), calc(100% - var(--var-pixel-size)) calc(100% - var(--var-pixel-size)), calc(100% - var(--var-pixel-size)) 100%, var(--var-pixel-size) 100%, var(--var-pixel-size) calc(100% - var(--var-pixel-size)), 0 calc(100% - var(--var-pixel-size)), 0 var(--var-pixel-size), var(--var-pixel-size) var(--var-pixel-size));
  // FIXME: remove this min-height after we figure out how to have initial height for zoomable images
  min-height: calc(var(--var-pixel-size) * 3);
}
`;

const BorderedContainer: FC<PropsWithChildren<BorderedContainerProps>> = ({
  children,
  className,
  renderAs,
  renderChildAs,
}) => (
  <Border className={className} as={renderAs}>
    <BorderedContent as={renderChildAs}>
      {children}
    </BorderedContent>
  </Border>
);

export default memo(BorderedContainer);
