import type { FC } from 'react';
import { memo, useState, useEffect } from 'react';
import styled from 'styled-components';
import { Link } from 'gatsby';
import Container from './Container';
import { FlexRow, FlexColumn } from './Flex';
import { ReactComponent as SiteLogo } from '../assets/images/logo.svg';
import { mediaQueries } from '../styles';
import routes from '../routes';
import { PageProps } from '../types';
import { ThemeToggle } from '.';

const HeaderWrapper = styled.div`
  background-color: var(--color-background);
  transition: var(--var-theme-toggle-transition);
  padding: 1.5rem 0;

  @media ${mediaQueries.mobileLandscape} {
    padding: 2rem 0;
  }

  @media ${mediaQueries.tabletDesktop} {
    padding: 3.375rem 0;
  }
`;

const HeaderLink = styled(Link)`
  display: inline-flex;
  justify-content: flex-start;
  align-items: center;
  color: var(--color-main-text);
  text-decoration: none;
  height: 2rem; /* IE fix */
  border-bottom: 0;
  vertical-align: middle;

  @media (pointer: fine) {
    &:hover {
      color: var(--color-main-text);
      border-bottom: 0;
    }
  }
`;

const Logo = styled(SiteLogo)`
  height: 2rem;
  width: auto;
  min-height: 2rem; /* IE fix */
  max-width: 2rem; /* IE fix */
  margin: 0 0.5rem 0 0;;
  border: 0;

  @media ${mediaQueries.mobilePortrait} {
    margin: 0;
  }
`;

const SiteName = styled.h1`
  font-size: 1.125rem;
  line-height: 1.375rem;
  margin: 0;
  white-space: nowrap;

  @media ${mediaQueries.tabletDesktop} {
    font-size: 1.375rem;
    line-height: 1.75rem;
  }

  @media ${mediaQueries.mobilePortrait} {
    display: none;
  }
`;

const SiteNav = styled.ul`
  list-style: none;
  padding: 0;
  margin: 0;
  display: flex;
  justify-content: flex-end;

  > li {
    font-size: 1.125rem;
    line-height: 1.375rem;
    padding: 0 0 0 1.5rem;

    @media ${mediaQueries.tabletDesktop} {
      font-size: 1.375rem;
      line-height: 1.75rem;
      padding: 0 0 0 2.125rem;
    }

    &:first-child {
      padding: 0;
    }

    &:before {
      content: unset;
    }

    a {
      color: var(--color-navigation-link);

      @media (pointer: fine) {
        &:hover {
          color: var(--color-navigation-link-hover);
        }
      }

      &.active {
        border-bottom: 0.125rem solid var(--color-navigation-link);

        @media (pointer: fine) {
          &:hover {
            border-bottom: 0.125rem solid var(--color-navigation-link-hover);
          }
        }
      }
    }
  }
`;

const Header: FC<Pick<PageProps, 'path'>> = ({ path }) => {
  // Set active only when path is ready in client
  const [isProjectPath, setIsProjectPath] = useState(false);
  const [isAboutPath, setIsAboutPath] = useState(false);
  useEffect(() => {
    setIsProjectPath(path.indexOf(routes.projectDetailsRoot) >= 0 || path === routes.home);
    // This is to workaround a bug where Gatsby doesn't seems to add active class
    // on initial page load
    setIsAboutPath(new RegExp(`^${routes.about}`).test(path));
  }, [path]);

  return (
    <HeaderWrapper>
      <Container>
        <FlexRow $justifyContent="center" $alignItems="center">
          <FlexColumn $xs={3} $sm={6}>
            <HeaderLink to={routes.home}>
              <Logo aria-label="icelam.github.io" />
              <SiteName>icelam.github.io</SiteName>
            </HeaderLink>
          </FlexColumn>

          <FlexColumn $xs={9} $sm={6}>
            <SiteNav>
              <li>
                <Link
                  to={routes.home}
                  activeClassName="active"
                  partiallyActive={isProjectPath}
                >
                  Projects
                </Link>
              </li>
              <li>
                <Link
                  to={routes.about}
                  activeClassName="active"
                  partiallyActive={isAboutPath}
                >
                  About
                </Link>
              </li>
              <li>
                <ThemeToggle />
              </li>
            </SiteNav>
          </FlexColumn>
        </FlexRow>
      </Container>
    </HeaderWrapper>
  );
};

export default memo(Header);
