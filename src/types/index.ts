import type { PropsWithChildren } from 'react';
import type { PageProps as GatsbyPageProps } from 'gatsby';

export * from './utilityTypes';

export type AvailableThemes = 'light' | 'dark';

export type LocationStateType = Record<string, never>;
export type PageProps<DataType = Record<string, never>> = PropsWithChildren<Omit<GatsbyPageProps<DataType, unknown, LocationStateType>, 'children'>>;

export type MdxPageSEOFrontmatter = Pick<Queries.MdxFrontmatter, 'pageTitle' | 'pageDescription' | 'pageKeywords'>;

export type MdxPageData<FrontmatterType = Record<string, never>> = {
  readonly mdx: {
    readonly frontmatter: FrontmatterType | null;
  } | null
};
