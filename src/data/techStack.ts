interface TechStack {
  [key: string]: {
    backgroundColor: string;
    textColor: string;
    name: string;
  }
}

const techStack: TechStack = {
  reactjs: {
    backgroundColor: '#61dafb',
    textColor: '#ffffff',
    name: 'ReactJS',
  },
  styledcomponent: {
    backgroundColor: '#db7093',
    textColor: '#ffffff',
    name: 'Styled component',
  },
  pwa: {
    backgroundColor: '#580ec3',
    textColor: '#ffffff',
    name: 'PWA',
  },
  nodejs: {
    backgroundColor: '#43853d',
    textColor: '#ffffff',
    name: 'Node.js',
  },
  express: {
    backgroundColor: '#259dff',
    textColor: '#ffffff',
    name: 'express',
  },
  redux: {
    backgroundColor: '#764abc',
    textColor: '#ffffff',
    name: 'Redux',
  },
  css3: {
    backgroundColor: '#1a70b5',
    textColor: '#ffffff',
    name: 'CSS3',
  },
  vuejs: {
    backgroundColor: '#4fc08d',
    textColor: '#ffffff',
    name: 'Vue.js',
  },
  webpack: {
    backgroundColor: '#62b1d8',
    textColor: '#ffffff',
    name: 'Webpack',
  },
  javascript: {
    backgroundColor: '#fbc700',
    textColor: '#ffffff',
    name: 'JavaScript',
  },
  html: {
    backgroundColor: '#de4b25',
    textColor: '#ffffff',
    name: 'HTML',
  },
  animation: {
    backgroundColor: '#666666',
    textColor: '#ffffff',
    name: 'Animation',
  },
  materialui: {
    backgroundColor: '#1976d2',
    textColor: '#ffffff',
    name: 'Material-UI',
  },
  workbox: {
    backgroundColor: '#fb8c00',
    textColor: '#ffffff',
    name: 'Workbox',
  },
  d3js: {
    backgroundColor: '#f9a03c',
    textColor: '#ffffff',
    name: 'D3.js',
  },
  php: {
    backgroundColor: '#4f5b93',
    textColor: '#ffffff',
    name: 'PHP',
  },
  graphql: {
    backgroundColor: '#d64292',
    textColor: '#ffffff',
    name: 'GraphQL',
  },
  pug: {
    backgroundColor: '#a86454',
    textColor: '#ffffff',
    name: 'Pug',
  },
  gulp: {
    backgroundColor: '#cf4647',
    textColor: '#ffffff',
    name: 'gulp.js',
  },
  webextension: {
    backgroundColor: '#1b367a',
    textColor: '#ffffff',
    name: 'Web Extension',
  },
  matterjs: {
    backgroundColor: '#f55f5e',
    textColor: '#ffffff',
    name: 'matter.js',
  },
  jest: {
    backgroundColor: '#15c213',
    textColor: '#ffffff',
    name: 'Jest',
  },
  storybook: {
    backgroundColor: '#ff4785',
    textColor: '#ffffff',
    name: 'Storybook',
  },
  enzyme: {
    backgroundColor: '#ff385c',
    textColor: '#ffffff',
    name: 'Enzyme',
  },
  esdoc: {
    backgroundColor: '#e55151',
    textColor: '#ffffff',
    name: 'ESDoc',
  },
  typescript: {
    backgroundColor: '#3178c6',
    textColor: '#ffffff',
    name: 'TypeScript',
  },
  deno: {
    backgroundColor: '#888888',
    textColor: '#ffffff',
    name: 'Deno',
  },
  electron: {
    backgroundColor: '#74b1b2',
    textColor: '#ffffff',
    name: 'Electron',
  },
  litelement: {
    backgroundColor: '#2196f3',
    textColor: '#ffffff',
    name: 'LitElement',
  },
  webAnimations: {
    backgroundColor: '#304a6b',
    textColor: '#ffffff',
    name: 'Web Animations API',
  },
  webAudio: {
    backgroundColor: '#e9562f',
    textColor: '#ffffff',
    name: 'Web Audio API',
  },
  python: {
    backgroundColor: '#3776ab',
    textColor: '#ffffff',
    name: 'Python',
  },
  serverless: {
    backgroundColor: '#fd5750',
    textColor: '#ffffff',
    name: 'Serverless Framework',
  },
  lambda: {
    backgroundColor: '#d16311',
    textColor: '#ffffff',
    name: 'AWS Lambda',
  },
};

export default techStack;
