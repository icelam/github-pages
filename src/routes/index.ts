const routes = {
  home: '/',
  about: '/about',
  projectDetailsRoot: '/project-details',
  projectDetails: '/project-details/:projectId',
  notFound: '/404',
};

export default routes;
