// React Framer Motion animations

import { AnimationProps } from 'framer-motion';

// This valus is also shared by gastby-browser.tsx to control delay of scoll restoration
export const fadeDuration = 0.3;

export const fadeInOutAnimation: AnimationProps = {
  initial: 'hide',
  variants: {
    show: { opacity: 1, pointerEvents: 'initial' },
    hide: { opacity: 0, pointerEvents: 'none' },
  },
  transition: {
    duration: fadeDuration,
  },
};
