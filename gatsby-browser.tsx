import './src/assets/css/index.scss';
import type { GatsbyBrowser } from 'gatsby';
// Styled-component client-side rendering code referenced from
// https://github.com/gatsbyjs/gatsby/blob/c4d2d0f2a8fb01b4c0832d89ee1933a44cb3ea12/packages/gatsby-plugin-styled-components/src/gatsby-browser.js
import { StyleSheetManager } from 'styled-components';
import smoothscroll from 'smoothscroll-polyfill';
import { ThemeProvider } from './src/providers';
import { Layout } from './src/components';
import { LocationStateType } from './src/types';
import { fadeDuration } from './src/animations';
import { displayWelcomeMessage } from './src/utils';

type WrapPageElement = GatsbyBrowser<Record<string, unknown>, Record<string, unknown>, LocationStateType>['wrapPageElement'];

// Wrap root element with theme context
// https://www.gatsbyjs.com/blog/2019-01-31-using-react-context-api-with-gatsby/
// From docs: The APIs wrapPageElement and wrapRootElement exist in both the SSR and browser APIs.
// You generally should implement the same components in both gatsby-ssr.js and gatsby-browser.js
// so that pages generated through SSR with Node.js are the same after being hydrated in the browser
// https://www.gatsbyjs.com/docs/reference/config-files/gatsby-ssr/
export const wrapRootElement: GatsbyBrowser['wrapRootElement'] = ({ element }) => (
  <StyleSheetManager enableVendorPrefixes>
    <ThemeProvider>
      {element}
    </ThemeProvider>
  </StyleSheetManager>
);

// Wrap page with common layout which will not be unmount when route change
export const wrapPageElement: WrapPageElement = ({ element, props: { location } }) => (
  <Layout path={location.pathname}>{element}</Layout>
);

// Delay scroll restoration until fade animation is finish
export const shouldUpdateScroll: GatsbyBrowser['shouldUpdateScroll'] = ({
  routerProps: { location },
  getSavedScrollPosition,
}) => {
  // Workaround for Gatsby not respecting custom scroll logic
  if (window.history.scrollRestoration) {
    window.history.scrollRestoration = 'manual';
  }

  // Add extra 25ms delay to make sure new page content is mounted
  // This can prevent incorrect scroll position restoration when navigating from a shorter page
  // back to a longer page that has scroll position > previous page length
  const scrollDelay = fadeDuration * 1000 + 50;
  const savedPosition = getSavedScrollPosition(location) || [0, 0];
  window.setTimeout(() => {
    window.scrollTo(...savedPosition);
  }, scrollDelay);

  return false;
};

// Called when the Gatsby browser runtime first starts
export const onClientEntry: GatsbyBrowser['onClientEntry'] = () => {
  // Polyfill for window.scrollTo({ behavior:'smooth' });
  smoothscroll.polyfill();

  // Show Easter egg!
  displayWelcomeMessage();
};
