import type { GatsbyConfig } from 'gatsby';
import autoprefixer from 'autoprefixer';

// eslint-disable-next-line @typescript-eslint/no-var-requires, import/no-extraneous-dependencies
require('dotenv').config({
  path: '.env',
});

const config: GatsbyConfig = {
  flags: {
    DEV_SSR: true,
    PARALLEL_SOURCING: true,
  },
  trailingSlash: 'always',
  siteMetadata: {
    title: process.env.GATSBY_SITE_TITLE,
    siteUrl: process.env.GATSBY_SITE_URL,
  },
  graphqlTypegen: true,
  plugins: [
    'gatsby-plugin-sass',
    {
      resolve: 'gatsby-plugin-postcss',
      options: {
        postCssPlugins: [
          autoprefixer(),
        ],
      },
    },
    'gatsby-plugin-image',
    {
      resolve: 'gatsby-plugin-svgr',
      options: {
        prettier: true,
        svgo: true,
        svgoConfig: {
          plugins: [
            {
              name: 'preset-default',
              params: {
                overrides: {
                  removeViewBox: false,
                },
              },
            },
            'removeDimensions',
          ],
        },
      },
    },
    'gatsby-plugin-sitemap',
    {
      resolve: 'gatsby-plugin-manifest',
      options: {
        name: process.env.GATSBY_SITE_TITLE,
        short_name: process.env.GATSBY_SITE_TITLE,
        description: process.env.GATSBY_SITE_DESCRIPTION,
        start_url: '.',
        display: 'standalone',
        theme_color: process.env.GATSBY_SITE_MANIFEST_THEME_COLOR,
        background_color: process.env.GATSBY_SITE_MANIFEST_BACKGROUND_COLOR,
        icon: 'static/images/icon-maskable.png',
        icon_options: {
          purpose: 'any maskable',
        },
        cache_busting_mode: 'none',
        include_favicon: false,
      },
    },
    {
      resolve: 'gatsby-plugin-mdx',
      options: {
        extensions: ['.md', '.mdx'],
        gatsbyRemarkPlugins: [
          {
            resolve: 'gatsby-remark-copy-linked-files',
            options: {
              // `ignoreFileExtensions` defaults to [`png`, `jpg`, `jpeg`, `bmp`, `tiff`]
              // as plugin assume user will use gatsby-remark-images to handle images in markdown
              // as it automatically creates responsive versions of images. Set
              // `ignoreFileExtensions` to an empty array to allow inline image.
              ignoreFileExtensions: [],
              destinationDir: 'static',
            },
          },
          {
            resolve: 'gatsby-remark-external-links',
            options: {
              target: '_blank',
              rel: 'noopener noreferrer',
            },
          },
        ],
      },
    },
    'gatsby-plugin-sharp',
    'gatsby-transformer-sharp',
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'images',
        path: './src/assets/images',
      },
      __key: 'images',
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'pages',
        path: './src/pages',
      },
      __key: 'pages',
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'about',
        path: `${__dirname}/contents/about`,
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'projects',
        path: `${__dirname}/contents/projects`,
      },
    },
    {
      resolve: 'gatsby-plugin-google-tagmanager',
      options: {
        id: process.env.GATSBY_GOOGLE_TAG_MANAGER_ID,
        includeInDevelopment: true,
      },
    },
    'gatsby-plugin-remove-serviceworker',
  ],
};

export default config;
