# icelam.github.io

![Build Status](https://travis-ci.com/icelam/github-page.svg?token=UGEFFUtYUq9ydEraXxxz&branch=master)

Source code for [icelam.github.io](https://icelam.github.io). Auto deploy by Travis CI.

## Install packages need for the project

Install npm packages in project root folder first using `yarn`.

## To start the project

Run `yarn start` in project root folder.

## Refresh content

Run `yarn refresh-content` in project root folder.  
Reference: [https://www.gatsbyjs.com/docs/refreshing-content/](https://www.gatsbyjs.com/docs/refreshing-content/)

## Node version

This project is developed using Gatsby v5 which requires Node >= 18.
