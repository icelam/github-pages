import './src/assets/css/index.scss';
import type { GatsbySSR } from 'gatsby';
// Styled-component server-side rendering code referenced from
// https://github.com/gatsbyjs/gatsby/blob/c4d2d0f2a8fb01b4c0832d89ee1933a44cb3ea12/packages/gatsby-plugin-styled-components/src/gatsby-ssr.js
import { ServerStyleSheet, StyleSheetManager } from 'styled-components';
import { ThemeProvider } from './src/providers';
import { Layout } from './src/components';
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
// eslint-disable-next-line import/no-webpack-loader-syntax, import/extensions, import/no-unresolved
import ssrThemeHandler from '!raw-loader!remove-comments-loader!./src/ssr-theme-handler.js';

const sheetByPathname = new Map();

/*
 * Script to handle dark mode perfectly on SSR, based on:
 * - https://github.com/gaearon/overreacted.io/blob/2e810e189ba30c9825b989c3cf678a491ee2c1c3/src/html.js#L21
 * - https://victorzhou.com/blog/dark-mode-gatsby/#putting-it-together
 *
 * Background:
 * When using styled-components with ThemeProvider to change theme colors, the initial theme
 * has to be default to 'light' theme. This is due to the fact that server side renderer do
 * not have access to window object or local storage, and it also doesn't know the browser
 * setting of users. Thus we have to produce code like this in root component:
 *
 * // Note that we have to hard code `theme` to 'light' for SSR to work properly
 * // Any attempt to access window.matchMedia or local storage will cause build errors
 * const [theme, setTheme] = useState<AvailableThemes>('light');
 *
 * useEffect(() => {
 *   setTheme(detectColorScheme());
 * }, []);
 *
 * return (
 *   <SCThemeProvider theme={themes[theme]}>
 *     <GlobalStyle />
 *     {children}
 *   </SCThemeProvider>
 * );
 *
 * However, this causes browser to deliver the light theme at initial load, leading to
 * flickering. To resolve the issue, we have to detect the color scheme before components
 * are rendered, and switch to use CSS variables instead of styled-components theme.
 * Here is an article which explains the issue: https://www.joshwcomeau.com/react/dark-mode/
 */
export const onRenderBody: GatsbySSR['onRenderBody'] = ({ setHeadComponents, pathname }) => {
  const sheet = sheetByPathname.get(pathname);
  if (sheet) {
    setHeadComponents([sheet.getStyleElement()]);
    sheetByPathname.delete(pathname);
  }

  setHeadComponents([
    <script
      key="ssrThemeHandler"
      // eslint-disable-next-line react/no-danger
      dangerouslySetInnerHTML={{ __html: ssrThemeHandler.replace(/\n/g, ' ').replace(/ {2}/g, '') }}
    />,
  ]);
};

// Wrap root element with theme context
// https://www.gatsbyjs.com/blog/2019-01-31-using-react-context-api-with-gatsby/
// From docs: The APIs wrapPageElement and wrapRootElement exist in both the SSR and browser APIs.
// You generally should implement the same components in both gatsby-ssr.js and gatsby-browser.js
// so that pages generated through SSR with Node.js are the same after being hydrated in the browser
// https://www.gatsbyjs.com/docs/reference/config-files/gatsby-ssr/
export const wrapRootElement: GatsbySSR['wrapRootElement'] = ({ element, pathname }) => {
  const sheet = new ServerStyleSheet();
  sheetByPathname.set(pathname, sheet);

  return (
    <StyleSheetManager sheet={sheet.instance} enableVendorPrefixes>
      <ThemeProvider>
        {element}
      </ThemeProvider>
    </StyleSheetManager>
  );
};

// Wrap page with common layout which will not be unmount when route change
export const wrapPageElement: GatsbySSR['wrapPageElement'] = ({ element, props: { location } }) => (
  <Layout path={location.pathname}>{element}</Layout>
);
